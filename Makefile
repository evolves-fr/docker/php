########################################################################################################################
# Global variables
########################################################################################################################
REGISTRY_IMAGE := "registry.gitlab.com/evolves-fr/docker/php"
REVISION := "1"
CREATED_AT := "$$(date -Iseconds)"

########################################################################################################################
# Definition
########################################################################################################################
define build_docker_image
	docker build \
		--platform linux/amd64,linux/arm64 \
		--provenance=false \
		--tag ${REGISTRY_IMAGE}:${TAG_MAJOR} \
		--tag ${REGISTRY_IMAGE}:${TAG_MINOR} \
		--tag ${REGISTRY_IMAGE}:${TAG_MINOR}-${REVISION} \
		--build-arg VERSION=${TAG_MINOR} \
		--build-arg REVISION=${REVISION} \
		--build-arg CREATED_AT=${CREATED_AT} \
		--build-arg GO_TAG=${GO_TAG} \
		--build-arg COMPOSER_TAG=${COMPOSER_TAG} \
		--build-arg PHP_TAG=${PHP_TAG} \
		--build-arg CA_CERTIFICATES_VERSION=${CA_CERTIFICATES_VERSION} \
		--build-arg LIBICU_DEV_VERSION=${LIBICU_DEV_VERSION} \
		--build-arg LIBZIP_DEV_VERSION=${LIBZIP_DEV_VERSION} \
		--build-arg LIBZSTD_DEV_VERSION=${LIBZSTD_DEV_VERSION} \
		--build-arg LIBGMP_DEV_VERSION=${LIBGMP_DEV_VERSION} \
		--build-arg LIBMEMCACHED_DEV_VERSION=${LIBMEMCACHED_DEV_VERSION} \
		--build-arg NGINX_VERSION=${NGINX_VERSION} \
		--build-arg PROCPS_VERSION=${PROCPS_VERSION} \
		--build-arg CACHETOOL_VERSION=${CACHETOOL_VERSION} \
		--build-arg EXT_IGBINARY_VERSION=${EXT_IGBINARY_VERSION} \
		--build-arg EXT_ZSTD_VERSION=${EXT_ZSTD_VERSION} \
		--build-arg EXT_REDIS_VERSION=${EXT_REDIS_VERSION} \
		--build-arg EXT_MEMCACHE_VERSION=${EXT_MEMCACHE_VERSION} \
		--build-arg EXT_MEMCACHE_CFLAG=${EXT_MEMCACHE_CFLAG} \
		--build-arg EXT_XDEBUG_VERSION=${EXT_XDEBUG_VERSION} \
		--progress=plain \
		--no-cache \
		--push \
		.
endef

########################################################################################################################
# Default
########################################################################################################################
.PHONY: all
all: 5.6 7.0 7.1 7.2 7.3 7.4 8.0 8.1 8.2 8.3 8.4

########################################################################################################################
# 5.6 @deprecated
########################################################################################################################
.PHONY: 5.6
5.6: TAG_MAJOR="5.6"
5.6: TAG_MINOR="5.6.40"
5.6: GO_TAG="1.18-stretch"
5.6: PHP_TAG="5.6.40-fpm-stretch"
5.6: COMPOSER_TAG="2.2.16"
5.6: CA_CERTIFICATES_VERSION="20200601~deb9u2"
5.6: LIBICU_DEV_VERSION="57.1-6+deb9u5"
5.6: LIBZIP_DEV_VERSION="1.1.2-1.1+deb9u1"
5.6: LIBZSTD_DEV_VERSION="1.1.2-1+deb9u1"
5.6: LIBGMP_DEV_VERSION="2:6.1.2+dfsg-1+deb9u1"
5.6: LIBMEMCACHED_DEV_VERSION="1.0.18-4.1"
5.6: NGINX_VERSION="1.10.3-1+deb9u7"
5.6: PROCPS_VERSION="2:3.3.12-3+deb9u1"
5.6: CACHETOOL_VERSION="3.2.2"
5.6: EXT_IGBINARY_VERSION="2.0.8"
5.6: EXT_ZSTD_VERSION="0.10.0"
5.6: EXT_REDIS_VERSION="4.3.0"
5.6: EXT_MEMCACHE_VERSION="3.0.8"
5.6: EXT_MEMCACHE_CFLAG="-fgnu89-inline"
5.6: EXT_XDEBUG_VERSION="2.5.5"
5.6:
	@$(build_docker_image)

########################################################################################################################
# 7.0 @deprecated
########################################################################################################################
.PHONY: 7.0
7.0: TAG_MAJOR="7.0"
7.0: TAG_MINOR="7.0.33"
7.0: GO_TAG="1.18-stretch"
7.0: PHP_TAG="7.0.33-fpm-stretch"
7.0: COMPOSER_TAG="2.2.16"
7.0: CA_CERTIFICATES_VERSION="20200601~deb9u2"
7.0: LIBICU_DEV_VERSION="57.1-6+deb9u5"
7.0: LIBZIP_DEV_VERSION="1.1.2-1.1+deb9u1"
7.0: LIBZSTD_DEV_VERSION="1.1.2-1+deb9u1"
7.0: LIBGMP_DEV_VERSION="2:6.1.2+dfsg-1+deb9u1"
7.0: LIBMEMCACHED_DEV_VERSION="1.0.18-4.1"
7.0: NGINX_VERSION="1.10.3-1+deb9u7"
7.0: PROCPS_VERSION="2:3.3.12-3+deb9u1"
7.0: CACHETOOL_VERSION="3.2.2"
7.0: EXT_IGBINARY_VERSION="3.2.7"
7.0: EXT_ZSTD_VERSION="0.10.0"
7.0: EXT_REDIS_VERSION="4.3.0"
7.0: EXT_MEMCACHE_VERSION="4.0.5.2"
7.0: EXT_MEMCACHE_CFLAG=""
7.0: EXT_XDEBUG_VERSION="2.7.2"
7.0:
	@$(build_docker_image)

########################################################################################################################
# 7.1 @deprecated
########################################################################################################################
.PHONY: 7.1
7.1: TAG_MAJOR="7.1"
7.1: TAG_MINOR="7.1.33"
7.1: GO_TAG="1.18-buster"
7.1: PHP_TAG="7.1.33-fpm-buster"
7.1: COMPOSER_TAG="2.2.16"
7.1: CA_CERTIFICATES_VERSION="20200601~deb10u2"
7.1: LIBICU_DEV_VERSION="63.1-6+deb10u3"
7.1: LIBZIP_DEV_VERSION="1.5.1-4"
7.1: LIBZSTD_DEV_VERSION="1.3.8+dfsg-3+deb10u2"
7.1: LIBGMP_DEV_VERSION="2:6.1.2+dfsg-4+deb10u1"
7.1: LIBMEMCACHED_DEV_VERSION="1.0.18-4.2"
7.1: NGINX_VERSION="1.14.2-2+deb10u5"
7.1: PROCPS_VERSION="2:3.3.15-2"
7.1: CACHETOOL_VERSION="4.1.0"
7.1: EXT_IGBINARY_VERSION="3.2.7"
7.1: EXT_ZSTD_VERSION="0.11.0"
7.1: EXT_REDIS_VERSION="5.3.7"
7.1: EXT_MEMCACHE_VERSION="4.0.5.2"
7.1: EXT_MEMCACHE_CFLAG=""
7.1: EXT_XDEBUG_VERSION="2.9.8"
7.1:
	@$(build_docker_image)

########################################################################################################################
# 7.2 @deprecated
########################################################################################################################
.PHONY: 7.2
7.2: TAG_MAJOR="7.2"
7.2: TAG_MINOR="7.2.34"
7.2: GO_TAG="1.18-buster"
7.2: PHP_TAG="7.2.34-fpm-buster"
7.2: COMPOSER_TAG="2.3.9"
7.2: CA_CERTIFICATES_VERSION="20200601~deb10u2"
7.2: LIBICU_DEV_VERSION="63.1-6+deb10u3"
7.2: LIBZIP_DEV_VERSION="1.5.1-4"
7.2: LIBZSTD_DEV_VERSION="1.3.8+dfsg-3+deb10u2"
7.2: LIBGMP_DEV_VERSION="2:6.1.2+dfsg-4+deb10u1"
7.2: LIBMEMCACHED_DEV_VERSION="1.0.18-4.2"
7.2: NGINX_VERSION="1.14.2-2+deb10u5"
7.2: PROCPS_VERSION="2:3.3.15-2"
7.2: CACHETOOL_VERSION="5.1.3"
7.2: EXT_IGBINARY_VERSION="3.2.7"
7.2: EXT_ZSTD_VERSION="0.11.0"
7.2: EXT_REDIS_VERSION="5.3.7"
7.2: EXT_MEMCACHE_VERSION="4.0.5.2"
7.2: EXT_MEMCACHE_CFLAG=""
7.2: EXT_XDEBUG_VERSION="3.1.6"
7.2:
	@$(build_docker_image)

########################################################################################################################
# 7.3 @deprecated
########################################################################################################################
.PHONY: 7.3
7.3: TAG_MAJOR="7.3"
7.3: TAG_MINOR="7.3.33"
7.3: GO_TAG="1.18-bullseye"
7.3: PHP_TAG="7.3.33-fpm-bullseye"
7.3: COMPOSER_TAG="2.3.9"
7.3: CA_CERTIFICATES_VERSION="20210119"
7.3: LIBICU_DEV_VERSION="67.1-7"
7.3: LIBZIP_DEV_VERSION="1.7.3-1"
7.3: LIBZSTD_DEV_VERSION="1.4.8+dfsg-2.1"
7.3: LIBGMP_DEV_VERSION="2:6.2.1+dfsg-1+deb11u1"
7.3: LIBMEMCACHED_DEV_VERSION="1.0.18-4.2"
7.3: NGINX_VERSION="1.18.0-6.1+deb11u3"
7.3: PROCPS_VERSION="2:3.3.17-5"
7.3: CACHETOOL_VERSION="7.1.0"
7.3: EXT_IGBINARY_VERSION="3.2.7"
7.3: EXT_ZSTD_VERSION="0.11.0"
7.3: EXT_REDIS_VERSION="5.3.7"
7.3: EXT_MEMCACHE_VERSION="4.0.5.2"
7.3: EXT_MEMCACHE_CFLAG=""
7.3: EXT_XDEBUG_VERSION="3.1.6"
7.3:
	@$(build_docker_image)

########################################################################################################################
# 7.4 @deprecated
########################################################################################################################
.PHONY: 7.4
7.4: TAG_MAJOR="7.4"
7.4: TAG_MINOR="7.4.33"
7.4: GO_TAG="1.18-bullseye"
7.4: PHP_TAG="7.4.33-fpm-bullseye"
7.4: COMPOSER_TAG="2.3.9"
7.4: CA_CERTIFICATES_VERSION="20210119"
7.4: LIBICU_DEV_VERSION="67.1-7"
7.4: LIBZIP_DEV_VERSION="1.7.3-1"
7.4: LIBZSTD_DEV_VERSION="1.4.8+dfsg-2.1"
7.4: LIBGMP_DEV_VERSION="2:6.2.1+dfsg-1+deb11u1"
7.4: LIBMEMCACHED_DEV_VERSION="1.0.18-4.2"
7.4: NGINX_VERSION="1.18.0-6.1+deb11u3"
7.4: PROCPS_VERSION="2:3.3.17-5"
7.4: CACHETOOL_VERSION="7.1.0"
7.4: EXT_IGBINARY_VERSION="3.2.7"
7.4: EXT_ZSTD_VERSION="0.11.0"
7.4: EXT_REDIS_VERSION="5.3.7"
7.4: EXT_MEMCACHE_VERSION="4.0.5.2"
7.4: EXT_MEMCACHE_CFLAG=""
7.4: EXT_XDEBUG_VERSION="3.1.6"
7.4:
	@$(build_docker_image)

########################################################################################################################
# 8.0
########################################################################################################################
.PHONY: 8.0
8.0: TAG_MAJOR="8.0"
8.0: TAG_MINOR="8.0.30"
8.0: GO_TAG="1.21.1-bullseye"
8.0: PHP_TAG="8.0.30-fpm-bullseye"
8.0: COMPOSER_TAG="2.4.4"
8.0: CA_CERTIFICATES_VERSION="20210119"
8.0: LIBICU_DEV_VERSION="67.1-7"
8.0: LIBZIP_DEV_VERSION="1.7.3-1"
8.0: LIBZSTD_DEV_VERSION="1.4.8+dfsg-2.1"
8.0: LIBGMP_DEV_VERSION="2:6.2.1+dfsg-1+deb11u1"
8.0: LIBMEMCACHED_DEV_VERSION="1.0.18-4.2"
8.0: NGINX_VERSION="1.18.0-6.1+deb11u3"
8.0: PROCPS_VERSION="2:3.3.17-5"
8.0: CACHETOOL_VERSION="8.4.1"
8.0: EXT_IGBINARY_VERSION="3.2.12"
8.0: EXT_ZSTD_VERSION="0.12.0"
8.0: EXT_REDIS_VERSION="5.3.7"
8.0: EXT_MEMCACHE_VERSION="8.0"
8.0: EXT_MEMCACHE_CFLAG=""
8.0: EXT_XDEBUG_VERSION="3.4.1"
8.0:
	@$(build_docker_image)

########################################################################################################################
# 8.1
########################################################################################################################
.PHONY: 8.1
8.1: TAG_MAJOR="8.1"
8.1: TAG_MINOR="8.1.31"
8.1: GO_TAG="1.21.1-bullseye"
8.1: PHP_TAG="8.1.31-fpm-bullseye"
8.1: COMPOSER_TAG="2.5.8"
8.1: CA_CERTIFICATES_VERSION="20210119"
8.1: LIBICU_DEV_VERSION="67.1-7"
8.1: LIBZIP_DEV_VERSION="1.7.3-1"
8.1: LIBZSTD_DEV_VERSION="1.4.8+dfsg-2.1"
8.1: LIBGMP_DEV_VERSION="2:6.2.1+dfsg-1+deb11u1"
8.1: LIBMEMCACHED_DEV_VERSION="1.0.18-4.2"
8.1: NGINX_VERSION="1.18.0-6.1+deb11u3"
8.1: PROCPS_VERSION="2:3.3.17-5"
8.1: CACHETOOL_VERSION="9.0.3"
8.1: EXT_IGBINARY_VERSION="3.2.14"
8.1: EXT_ZSTD_VERSION="0.12.3"
8.1: EXT_REDIS_VERSION="5.3.7"
8.1: EXT_MEMCACHE_VERSION="8.2"
8.1: EXT_MEMCACHE_CFLAG=""
8.1: EXT_XDEBUG_VERSION="3.4.1"
8.1:
	@$(build_docker_image)

########################################################################################################################
# 8.2
########################################################################################################################
.PHONY: 8.2
8.2: TAG_MAJOR="8.2"
8.2: TAG_MINOR="8.2.27"
8.2: GO_TAG="1.21.1-bullseye"
8.2: PHP_TAG="8.2.27-fpm-bullseye"
8.2: COMPOSER_TAG="2.5.8"
8.2: CA_CERTIFICATES_VERSION="20210119"
8.2: LIBICU_DEV_VERSION="67.1-7"
8.2: LIBZIP_DEV_VERSION="1.7.3-1"
8.2: LIBZSTD_DEV_VERSION="1.4.8+dfsg-2.1"
8.2: LIBGMP_DEV_VERSION="2:6.2.1+dfsg-1+deb11u1"
8.2: LIBMEMCACHED_DEV_VERSION="1.0.18-4.2"
8.2: NGINX_VERSION="1.18.0-6.1+deb11u3"
8.2: PROCPS_VERSION="2:3.3.17-5"
8.2: CACHETOOL_VERSION="9.0.3"
8.2: EXT_IGBINARY_VERSION="3.2.14"
8.2: EXT_ZSTD_VERSION="0.12.3"
8.2: EXT_REDIS_VERSION="5.3.7"
8.2: EXT_MEMCACHE_VERSION="8.2"
8.2: EXT_MEMCACHE_CFLAG=""
8.2: EXT_XDEBUG_VERSION="3.4.1"
8.2:
	@$(build_docker_image)

########################################################################################################################
# 8.3
########################################################################################################################
.PHONY: 8.3
8.3: TAG_MAJOR="8.3"
8.3: TAG_MINOR="8.3.16"
8.3: GO_TAG="1.23.5-bookworm"
8.3: PHP_TAG="8.3.16-fpm-bookworm"
8.3: COMPOSER_TAG="2.8.5"
8.3: CA_CERTIFICATES_VERSION="20230311"
8.3: LIBICU_DEV_VERSION="72.1-3"
8.3: LIBZIP_DEV_VERSION="1.7.3-1+b1"
8.3: LIBZSTD_DEV_VERSION="1.5.4+dfsg2-5"
8.3: LIBGMP_DEV_VERSION="2:6.2.1+dfsg1-1.1"
8.3: LIBMEMCACHED_DEV_VERSION="1.1.4-1"
8.3: NGINX_VERSION="1.22.1-9"
8.3: PROCPS_VERSION="2:4.0.2-3"
8.3: CACHETOOL_VERSION="9.2.1"
8.3: EXT_IGBINARY_VERSION="3.2.16"
8.3: EXT_ZSTD_VERSION="0.14.0"
8.3: EXT_REDIS_VERSION="6.1.0"
8.3: EXT_MEMCACHE_VERSION="8.2"
8.3: EXT_MEMCACHE_CFLAG=""
8.3: EXT_XDEBUG_VERSION="3.4.1"
8.3:
	@$(build_docker_image)

########################################################################################################################
# 8.4
########################################################################################################################
.PHONY: 8.4
8.4: TAG_MAJOR="8.4"
8.4: TAG_MINOR="8.4.3"
8.4: GO_TAG="1.23.5-bookworm"
8.4: PHP_TAG="8.4.3-fpm-bookworm"
8.4: COMPOSER_TAG="2.8.5"
8.4: CA_CERTIFICATES_VERSION="20230311"
8.4: LIBICU_DEV_VERSION="72.1-3"
8.4: LIBZIP_DEV_VERSION="1.7.3-1+b1"
8.4: LIBZSTD_DEV_VERSION="1.5.4+dfsg2-5"
8.4: LIBGMP_DEV_VERSION="2:6.2.1+dfsg1-1.1"
8.4: LIBMEMCACHED_DEV_VERSION="1.1.4-1"
8.4: NGINX_VERSION="1.22.1-9"
8.4: PROCPS_VERSION="2:4.0.2-3"
8.4: CACHETOOL_VERSION="9.2.1"
8.4: EXT_IGBINARY_VERSION="3.2.16"
8.4: EXT_ZSTD_VERSION="0.14.0"
8.4: EXT_REDIS_VERSION="6.1.0"
8.4: EXT_MEMCACHE_VERSION="8.2"
8.4: EXT_MEMCACHE_CFLAG=""
8.4: EXT_XDEBUG_VERSION="3.4.1"
8.4:
	@$(build_docker_image)
