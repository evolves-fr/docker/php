package internal

import (
	"context"
	"net"
	"net/http"
	"sync"
	"syscall"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/sirupsen/logrus"
	"gitlab.com/evolves-fr/gommon"
)

type Exporter interface {
	Start() error
	Stop() error
	Wait()
	Run() error
	Addr() string
}

// NewExporter return Exporter interface
func NewExporter(address string, registry *prometheus.Registry) Exporter {
	// Initialize logger
	log := logrus.WithField("process", "metrics")

	// Initialize exporter server
	return &exporter{
		address: address,
		server: &http.Server{
			Addr: address,
			Handler: promhttp.HandlerFor(registry, promhttp.HandlerOpts{
				ErrorLog:           log,
				ErrorHandling:      promhttp.HTTPErrorOnError,
				DisableCompression: false,
				Timeout:            time.Second * 10,
				EnableOpenMetrics:  true,
			}),
		},
		wg:  new(sync.WaitGroup),
		log: log,
	}
}

// exporter implement Exporter interface
type exporter struct {
	address  string
	listener net.Listener
	server   *http.Server
	wg       *sync.WaitGroup
	log      *logrus.Entry
}

// Start the Prometheus exporter server
func (e *exporter) Start() error {
	e.wg.Add(1)

	var err error
	e.listener, err = net.Listen("tcp", e.address)
	if err != nil {
		return err
	}

	go func() {
		defer e.wg.Done()
		if err = e.server.Serve(e.listener); err != http.ErrServerClosed {
			e.log.Fatal(err)
		}
	}()

	return nil
}

// Stop the Prometheus exporter server
func (e *exporter) Stop() error {
	if err := e.server.Shutdown(context.Background()); err != nil {
		return err
	}

	return e.server.Close()
}

// Wait the Prometheus exporter server after Start
func (e *exporter) Wait() {
	e.wg.Wait()
}

// Run the Prometheus exporter server
func (e *exporter) Run() error {
	// Signal observer
	var sigs = gommon.Signal(syscall.SIGINT, syscall.SIGQUIT, syscall.SIGTERM, syscall.SIGTSTP)

	// Handle signal for stop processor
	go func() {
		<-sigs
		if err := e.Stop(); err != http.ErrServerClosed {
			e.log.Error(err)
		}
	}()

	// Start processor
	if err := e.Start(); err != nil && err != http.ErrServerClosed {
		return err
	}

	// Wait routines
	e.Wait()

	return nil
}

// Addr of Prometheus exporter
func (e *exporter) Addr() string {
	return e.listener.Addr().String()
}
