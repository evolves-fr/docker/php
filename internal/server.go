package internal

import (
	"bytes"
	"io"
	"os"
	"os/exec"
	"os/user"
	"path/filepath"
	"strings"
	"sync"
	"syscall"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/sirupsen/logrus"
	"gitlab.com/evolves-fr/docker/php/pkg/collectors"
	"gitlab.com/evolves-fr/gommon"
)

const (
	initLockFile              = "/usr/lib/docker/init.lock"
	configurationDirectory    = "/usr/lib/docker/config"
	sourcesDirectory          = "/opt/docker/www"
	preStartScriptsDirectory  = "/opt/docker/pre-start"
	postStartScriptsDirectory = "/opt/docker/post-start"
	wwwDataUID                = 33
	wwwDataGID                = 33
)

type Server interface {
	Init() error
	Start() error
	Stop()
	Wait()
	Run() error
	Exec(process, bin string, args ...string) error
}

// NewServer return Server interface
func NewServer() Server {
	// Nginx access log reader and writer
	nginxAccessLog, nginxStdout := io.Pipe()

	// Initialize Prometheus registry
	registry := prometheus.NewRegistry()

	// Register Prometheus collectors
	registry.MustRegister(
		collectors.Up("php"),
		collectors.NginxAccessLog("php", nginxAccessLog),
		collectors.NginxStatus("php"),
		collectors.PHPFPMStatus("php"),
		collectors.OPCacheStatus("php"),
		collectors.Smem("php"),
	)

	// Initialize server
	srv := new(server)

	// Initialize PHP-FPM
	var phpfpmArgs []string
	if v := os.Getenv("ENTRYPOINT_FPM_ARGS"); v != "" {
		phpfpmArgs = strings.Split(v, " ")
	}
	srv.phpfpm = exec.Command("php-fpm", phpfpmArgs...)
	srv.phpfpm.Env = os.Environ()
	srv.phpfpm.Stdout = logrus.WithField("process", "fpm").WriterLevel(logrus.InfoLevel)
	srv.phpfpm.Stderr = fpmStderr()
	srv.phpfpm.ExtraFiles = []*os.File{fpmAccessLog()}

	// Initialize Nginx
	srv.nginx = exec.Command("nginx", "-g", "daemon off;")
	srv.nginx.Env = os.Environ()
	srv.nginx.Stdout = nginxStdout
	srv.nginx.Stderr = logrus.WithField("process", "nginx").WriterLevel(logrus.ErrorLevel)

	// Initialize Prometheus exporter
	if gommon.MustParse[bool](os.Getenv("ENTRYPOINT_METRICS_ENABLED")) {
		srv.exporter = NewExporter("0.0.0.0:9000", registry)
	}

	return srv
}

// server implement Server interface
type server struct {
	phpfpm   *exec.Cmd
	nginx    *exec.Cmd
	exporter Exporter
}

// Init PHP-FPM and Nginx configuration files
func (s server) Init() error {
	var log = logrus.WithField("process", "entrypoint")

	if !pathIsExist(initLockFile) {
		if usr, err := user.Current(); err == nil {
			log.Debugf("User %s (%s:%s)", usr.Name, usr.Uid, usr.Gid)
		}

		// Copy sources
		if !directoryIsEmpty(sourcesDirectory) {
			var dst = gommon.Default(os.Getenv("NGINX_SERVER_ROOT"), "/var/www/html")
			log.Debugf("Copy '%s' to '%s'", sourcesDirectory, dst)
			if err := copyDirectory(sourcesDirectory, dst); err != nil {
				return err
			}
			if err := chownR(dst, wwwDataUID, wwwDataGID); err != nil {
				return err
			}
		}

		// Detect framework
		if sfVersion, err := symfonyVersion(); err == nil {
			log.Debugf("Symfony %s detected", sfVersion)
		}

		// Initialize configuration server
		for input, output := range map[string]string{
			"/php/php.ini.gotxt":          "/usr/local/etc/php/php.ini",
			"/php-fpm/php-fpm.conf.gotxt": "/usr/local/etc/php-fpm.conf",
			"/php-fpm/www.conf.gotxt":     "/usr/local/etc/php-fpm.d/www.conf",
			"/nginx/nginx.conf.gotxt":     "/etc/nginx/nginx.conf",
			"/nginx/server.conf.gotxt":    "/etc/nginx/sites-available/default.conf",
		} {
			log.Debugf("Load %s", output)

			// Read input file
			inputData, err := os.ReadFile(filepath.Join(configurationDirectory, input))
			if err != nil {
				return err
			}

			// Open output file
			outputFile, err := os.OpenFile(output, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0o644)
			if err != nil {
				return err
			}

			// Substitue input data to output file
			subst := gommon.NewSubstitution(bytes.NewReader(inputData))
			subst.Funcs(gommon.FuncMap())
			subst.Func("hasSymfony", hasSymfony)
			subst.Func("hasOldSymfony", hasOldSymfony)
			if err = subst.Execute(outputFile); err != nil {
				return err
			}

			// Close output file
			if err = outputFile.Close(); err != nil {
				return err
			}
		}

		// Create required directories
		if fpmListenDir := filepath.Dir(os.Getenv("FPM_WWW_LISTEN")); fpmListenDir != "." {
			if err := createDirectory(fpmListenDir, 0755); err != nil {
				return err
			}
		}

		// Create init lock file
		lockFile, err := os.Create(initLockFile)
		if err != nil {
			return err
		}
		_ = lockFile.Close()
	}

	return nil
}

// Start PHP-FPM and Nginx process
func (s server) Start() error {
	// Load FPM extra files
	s.phpfpm.ExtraFiles = append(s.phpfpm.ExtraFiles, extraFiles("fpm")...)

	// PreStart
	if err := scripts(preStartScriptsDirectory, "pre-start"); err != nil {
		return err
	}

	// Signal observer
	var sigs = gommon.Signal(syscall.SIGINT, syscall.SIGQUIT, syscall.SIGTERM, syscall.SIGTSTP)

	// Handle signal
	go func() {
		<-sigs
		s.Stop()
		os.Exit(0)
	}()

	// Start PHP-FPM
	if err := s.phpfpm.Start(); err != nil {
		return err
	}
	logrus.WithField("process", "fpm").Debugf("Started on PID %d", s.phpfpm.Process.Pid)

	// Start Nginx
	if err := s.nginx.Start(); err != nil {
		return err
	}
	logrus.WithField("process", "nginx").Debugf("Started on PID %d", s.nginx.Process.Pid)

	// Metrics
	if s.exporter != nil {
		if err := s.exporter.Start(); err != nil {
			return err
		}
		logrus.WithField("process", "metrics").Debugf("Started on %s", s.exporter.Addr())
	}

	// PostStart
	if err := scripts(postStartScriptsDirectory, "post-start"); err != nil {
		return err
	}

	return nil
}

// Stop PHP-FPM and Nginx process
func (s server) Stop() {
	if s.nginx != nil {
		if s.nginx.ProcessState == nil {
			logrus.WithField("process", "nginx").Debug("Shutdown")
			if err := s.nginx.Process.Signal(syscall.SIGINT); err != nil {
				logrus.WithField("process", "nginx").Error(err)
			}
		}
	}

	if s.phpfpm != nil {
		if s.phpfpm.ProcessState == nil {
			logrus.WithField("process", "fpm").Debug("Shutdown")
			if err := s.phpfpm.Process.Signal(syscall.SIGINT); err != nil {
				logrus.WithField("process", "php-fpm").Error(err)
			}
		}
	}

	if s.exporter != nil {
		logrus.WithField("process", "metrics").Debug("Shutdown")
		if err := s.exporter.Stop(); err != nil {
			logrus.WithField("process", "metrics").Error(err)
		}
	}
}

// Wait PHP-FPM and Nginx process
func (s server) Wait() {
	// Wait commands process
	var wg sync.WaitGroup

	// Exporter
	if s.exporter != nil {
		wg.Add(1)
		go func() {
			s.exporter.Wait()
			wg.Done()
		}()
	}

	// PHP-FPM
	if s.phpfpm != nil {
		wg.Add(1)
		go func() {
			if err := s.phpfpm.Wait(); err != nil {
				logrus.WithField("process", "fpm").Warn(err)
				os.Exit(1)
			}
			wg.Done()
		}()
	}

	// Nginx
	if s.nginx != nil {
		wg.Add(1)
		go func() {
			if err := s.nginx.Wait(); err != nil {
				logrus.WithField("process", "nginx").Warn(err)
				os.Exit(1)
			}
			wg.Done()
		}()
	}

	wg.Wait()
}

// Run is Start and Wait alias
func (s server) Run() error {
	if err := s.Start(); err != nil {
		return err
	}

	s.Wait()

	return nil
}

// Exec another command
func (s server) Exec(process, bin string, args ...string) error {
	return execCommand(process, 0, 0, os.Stdin, bin, args...)
}
