package internal

import (
	"bufio"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/fs"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"
	"syscall"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/evolves-fr/gommon"
)

// File permissions
const (
	PermOtherExecute = 1 << iota // o+x
	PermOtherWrite               // o+w
	PermOtherRead                // o+r
	PermGroupExecute             // g+x
	PermGroupWrite               // g+w
	PermGroupRead                // g+o
	PermUserExecute              // u+x
	PermUserWrite                // u+w
	PermUserRead                 // u+r
)

var bufferLimit int64 = 1024

func init() {
	if v := os.Getenv("ENTRYPOINT_LOG_BUFFER_LIMIT"); v != "" {
		if i, err := strconv.ParseInt(v, 10, 64); err == nil && i > 0 {
			bufferLimit = i
		}
	}
}

func fpmAccessLog() *os.File {
	r, w, err := os.Pipe()
	if err != nil {
		return nil
	}

	go func() {
		defer w.Close()
		defer r.Close()

		scanner := bufio.NewScanner(r)

		for scanner.Scan() {
			var access map[string]any
			if err = json.Unmarshal(scanner.Bytes(), &access); err != nil {
				return
			}

			logrus.WithField("process", "fpm").WithFields(access).Log(logrus.InfoLevel)
		}

		if err = scanner.Err(); err != nil && err != io.EOF {
			return
		}
	}()

	return w
}

func fpmStderr() io.Writer {
	r, w := io.Pipe()

	//rxp := regexp.MustCompile(`\[([^]]+)]\s+WARNING:\s+\[pool\s+(\w+)]\s+child\s+(\d+)\s+said\s+into\s+(\w+):\s+"([^"]+)"`)

	go func() {
		defer w.Close()
		defer r.Close()

		scanner := bufio.NewScanner(r)
		for scanner.Scan() {
			//if rxp.Match(scanner.Bytes()) {
			//	s := rxp.FindStringSubmatch(scanner.Text())
			//
			//	logrus.WithFields(logrus.Fields{
			//		"process": "fpm",
			//		"pool":    s[2],
			//		"child":   s[3],
			//		"output":  s[4],
			//	}).Error(s[5])
			//} else {
			logrus.WithField("process", "fpm").Warn(scanner.Text())
			//}
		}

		if err := scanner.Err(); err != nil && err != io.EOF {
			return
		}
	}()

	return w
}

func extraFile(process, path string, level logrus.Level) *os.File {
	if gommon.Empty(path) {
		return nil
	}

	file, err := os.OpenFile(path, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0o666)
	if err != nil {
		return nil
	}

	writer := logrus.WithField("process", process).WithField("log", path).WriterLevel(level)

	go func() {
		defer file.Close()

		buf := make([]byte, bufferLimit)
		for {
			n, err := file.Read(buf)
			if err != nil && err != io.EOF {
				logrus.Error(err)
				break
			}

			if n == 0 {
				time.Sleep(time.Millisecond * 250)
				continue
			}

			if _, err = writer.Write(buf[:n]); err != nil {
				logrus.Error(err)
				break
			}
		}
	}()

	return file
}

func extraFiles(process string) []*os.File {
	var logFiles = make([]*os.File, 0)

	for _, dir := range strings.Split(strings.TrimSpace(os.Getenv("ENTRYPOINT_EXTRA_FILES")), ";") {
		if dir == "" {
			continue
		}

		if err := filepath.Walk(dir, func(path string, info fs.FileInfo, err error) error {
			if err != nil {
				return err
			}

			if !info.IsDir() {
				logrus.WithField("process", "entrypoint").Debugf("Load log file %s", path)
				logFiles = append(logFiles, extraFile(process, path, logrus.WarnLevel))
			}

			return nil
		}); err != nil {
			logrus.WithField("process", "entrypoint").Errorf("CustomLog %s : %v", dir, err)
		}
	}

	return logFiles
}

func execCommand(process string, uid, gid uint32, stdin io.Reader, bin string, args ...string) error {
	log := logrus.NewEntry(logrus.StandardLogger()).WithField("process", process)

	// Initialize command execution
	cmd := exec.Command(bin, args...)
	cmd.SysProcAttr = &syscall.SysProcAttr{Credential: &syscall.Credential{Uid: uid, Gid: gid}}
	cmd.Env = os.Environ()
	cmd.Stdin = stdin
	cmd.Stdout = log.WriterLevel(logrus.InfoLevel)
	cmd.Stderr = log.WriterLevel(logrus.WarnLevel)
	cmd.ExtraFiles = extraFiles(process)

	// Signal observer
	sigs := gommon.Signal(syscall.SIGINT, syscall.SIGQUIT, syscall.SIGTERM, syscall.SIGTSTP)

	// Handle signal
	go func() {
		sig := <-sigs
		if err := cmd.Process.Signal(sig); err != nil {
			log.Warn(err)
		}
		os.Exit(0)
	}()

	// Execute command
	return cmd.Run()
}

// scripts execute in command all files in directory
func scripts(path, process string) error {
	var log = logrus.WithField("process", process)

	if _, err := os.Stat(path); os.IsNotExist(err) {
		return nil
	}

	return filepath.Walk(path, func(path string, info fs.FileInfo, err error) error {
		// Check has error or is directory
		if err != nil || info.IsDir() {
			return err
		}

		// Ignore masked files
		if strings.HasPrefix(info.Name(), ".") {
			return nil
		}

		// Check if script is executable
		if info.Mode().Perm()&(PermUserExecute|PermGroupExecute|PermOtherExecute) == 0 {
			log.Warnf("%s is not executable", path)
			return nil
		}

		// Exec script
		log.Debugf("Running %s", path)
		return execCommand(process, wwwDataUID, wwwDataGID, nil, path)
	})
}

// symfonyConsolePath find console path in local directory
func symfonyConsolePath() (string, error) {
	var (
		basePath = filepath.Clean(gommon.Default(os.Getenv("NGINX_SERVER_ROOT"), "/var/www/html"))
		newPath  = basePath + "/bin/console"
		oldPath  = basePath + "/app/console"
	)

	if _, err := os.Stat(newPath); !os.IsNotExist(err) {
		return newPath, nil
	}

	if _, err := os.Stat(oldPath); !os.IsNotExist(err) {
		return oldPath, nil
	}

	return "", errors.New("symfony console path not found")
}

// symfonyVersion get version from Symfony CLI console
func symfonyVersion() (string, error) {
	// Get Symfony console path
	console, err := symfonyConsolePath()
	if err != nil {
		return "", err
	}

	// Initialize command context with timeout
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	// Get Symfony version
	cmd := exec.CommandContext(ctx, "php", console, "--version")
	cmd.SysProcAttr = &syscall.SysProcAttr{Credential: &syscall.Credential{Uid: wwwDataUID, Gid: wwwDataGID}}
	cmd.Env = os.Environ()
	out, err := cmd.Output()
	if ctx.Err() != nil && ctx.Err() != context.DeadlineExceeded {
		return "", ctx.Err()
	}
	if err != nil {
		return "", err
	}

	// Decode output
	var res = regexp.MustCompile(`(?m)^(Symfony).*(\d+.\d+.\d+).*$`).FindStringSubmatch(string(out))
	if len(res) >= 3 {
		if framework, version := res[1], res[2]; strings.EqualFold(framework, "Symfony") && len(version) > 0 {
			return version, nil
		}
	}

	return "", errors.New("can't get symfony version")
}

// hasOldSymfony check if version of Symfony is >=2 && <4
func hasOldSymfony() bool {
	version, err := symfonyVersion()
	if err != nil {
		return false
	}

	check, err := gommon.SemverCompare(">= 2, < 4", version)
	if err != nil {
		return false
	}

	return check
}

// hasSymfony check if version of Symfony is >=4
func hasSymfony() bool {
	version, err := symfonyVersion()
	if err != nil {
		return false
	}

	check, err := gommon.SemverCompare(">= 4", version)
	if err != nil {
		return false
	}

	return check
}

// PathIsExist verification
func pathIsExist(path string) bool {
	_, err := os.Stat(path)
	return !os.IsNotExist(err)
}

// DirectoryIsEmpty check directory has files
func directoryIsEmpty(path string) bool {
	if dir, err := ioutil.ReadDir(path); err == nil && len(dir) > 0 {
		return false
	}

	return true
}

// CreateDirectory if not exist
func createDirectory(dst string, perm os.FileMode) error {
	if pathIsExist(dst) {
		return nil
	}

	return os.MkdirAll(dst, perm)
}

// CopyDirectory files into another directory
func copyDirectory(src, dst string) error {
	entries, err := ioutil.ReadDir(src)
	if err != nil {
		return err
	}

	for _, entry := range entries {
		var (
			srcPath = filepath.Join(src, entry.Name())
			dstPath = filepath.Join(dst, entry.Name())
		)

		fileInfo, err := os.Stat(srcPath)
		if err != nil {
			return err
		}

		stat, ok := fileInfo.Sys().(*syscall.Stat_t)
		if !ok {
			return fmt.Errorf("failed to get raw syscall.Stat_t data for '%s'", srcPath)
		}

		switch fileInfo.Mode() & os.ModeType {
		case os.ModeDir:
			if err = createDirectory(dstPath, fileInfo.Mode()); err != nil {
				return err
			}
			if err = copyDirectory(srcPath, dstPath); err != nil {
				return err
			}
		case os.ModeSymlink:
			if err = copySymLink(srcPath, dstPath); err != nil {
				return err
			}
		default:
			if err = copyFile(srcPath, dstPath); err != nil {
				return err
			}
		}

		if err = os.Lchown(dstPath, int(stat.Uid), int(stat.Gid)); err != nil {
			return err
		}

		isSymlink := entry.Mode()&os.ModeSymlink != 0
		if !isSymlink {
			if err := os.Chmod(dstPath, entry.Mode()); err != nil {
				return err
			}
		}
	}

	return nil
}

// CopyFile into another directory
func copyFile(src, dst string) error {
	out, err := os.Create(dst)
	if err != nil {
		return err
	}
	defer out.Close()

	in, err := os.Open(src)
	if err != nil {
		return err
	}
	defer in.Close()

	if _, err = io.Copy(out, in); err != nil {
		return err
	}

	return nil
}

// CopySymLink into another directory
func copySymLink(src, dst string) error {
	link, err := os.Readlink(src)
	if err != nil {
		return err
	}

	return os.Symlink(link, dst)
}

// ChownR changes the numeric uid and gid of recursive directory.
func chownR(path string, uid, gid int) error {
	return filepath.Walk(path, func(path string, info fs.FileInfo, err error) error {
		if err != nil {
			return err
		}

		return os.Chown(path, uid, gid)
	})
}
