3625# PHP docker image with PHP-FPM, Nginx and dedicated entrypoint

## Installation

Use [Makefile](Makefile) for build docker images

| Version | Command    |
|---------|------------|
| `8.2`   | `make 8.2` |
| `8.1`   | `make 8.1` |
| `8.0`   | `make 8.0` |
| `7.4`   | `make 7.4` |
| `7.3`   | `make 7.3` |
| `7.2`   | `make 7.2` |
| `7.1`   | `make 7.1` |
| `7.0`   | `make 7.0` |
| `5.6`   | `make 5.6` |

## [Versions](https://www.php.net/supported-versions.php)

|                  | 8.2                    | 8.1                    | 8.0                    | 7.4                    | 7.3                    | 7.2                    | 7.1                    | 7.0                   | 5.6                   |
|------------------|------------------------|------------------------|------------------------|------------------------|------------------------|------------------------|------------------------|-----------------------|-----------------------|
| PHP              | 8.2.11                 | 8.1.24                 | 8.0.30                 | 7.4.33                 | 7.3.33                 | 7.2.34                 | 7.1.33                 | 7.0.33                | 5.6.40                |
| Status           | **Active**             | **Active**             | **Security**           | **End of life**        | **End of life**        | **End of life**        | **End of life**        | **End of life**       | **End of life**       |
| Active Support   | 08 Dec 2024            | 25 Nov 2023            | 26 Nov 2022            | 28 Nov 2021            | 06 Dec 2020            | 30 Nov 2019            | 01 Dec 2018            | 04 Jan 2018           | 19 Jan 2017           |
| Security Support | 08 Dec 2025            | 25 Nov 2024            | 26 Nov 2023            | 28 Nov 2022            | 06 Dec 2021            | 30 Nov 2020            | 01 Dec 2019            | 10 Jan 2019           | 31 Dec 2018           |
| OS               | Debian                 | Debian                 | Debian                 | Debian                 | Debian                 | Debian                 | Debian                 | Debian                | Debian                |
| CodeName         | Bullseye               | Bullseye               | Bullseye               | Bullseye               | Bullseye               | Buster                 | Buster                 | Stretch               | Stretch               |
| CA Certs         | 20210119               | 20210119               | 20210119               | 20210119               | 20210119               | 20200601~deb10u2       | 20200601~deb10u2       | 20200601~deb9u2       | 20200601~deb9u2       |
| Lib ICU          | 67.1-7                 | 67.1-7                 | 67.1-7                 | 67.1-7                 | 67.1-7                 | 63.1-6+deb10u3         | 63.1-6+deb10u3         | 57.1-6+deb9u5         | 57.1-6+deb9u5         |
| Lib ZIP          | 1.7.3-1                | 1.7.3-1                | 1.7.3-1                | 1.7.3-1                | 1.7.3-1                | 1.5.1-4                | 1.5.1-4                | 1.1.2-1.1+deb9u1      | 1.1.2-1.1+deb9u1      |
| Lib ZSTD         | 1.4.8+dfsg-2.1         | 1.4.8+dfsg-2.1         | 1.4.8+dfsg-2.1         | 1.4.8+dfsg-2.1         | 1.4.8+dfsg-2.1         | 1.3.8+dfsg-3+deb10u2   | 1.3.8+dfsg-3+deb10u2   | 1.1.2-1+deb9u1        | 1.1.2-1+deb9u1        |
| Lib GMP          | 2:6.2.1+dfsg-1+deb11u1 | 2:6.2.1+dfsg-1+deb11u1 | 2:6.2.1+dfsg-1+deb11u1 | 2:6.2.1+dfsg-1+deb11u1 | 2:6.2.1+dfsg-1+deb11u1 | 2:6.1.2+dfsg-4+deb10u1 | 2:6.1.2+dfsg-4+deb10u1 | 2:6.1.2+dfsg-1+deb9u1 | 2:6.1.2+dfsg-1+deb9u1 |
| Lib MemCached    | 1.0.18-4.2             | 1.0.18-4.2             | 1.0.18-4.2             | 1.0.18-4.2             | 1.0.18-4.2             | 1.0.18-4.2             | 1.0.18-4.2             | 1.0.18-4.1            | 1.0.18-4.1            |
| Nginx            | 1.18.0-6.1+deb11u3     | 1.18.0-6.1+deb11u3     | 1.18.0-6.1+deb11u3     | 1.18.0-6.1+deb11u3     | 1.18.0-6.1+deb11u3     | 1.14.2-2+deb10u5       | 1.14.2-2+deb10u5       | 1.10.3-1+deb9u7       | 1.10.3-1+deb9u7       |
| Procps           | 2:3.3.17-5             | 2:3.3.17-5             | 2:3.3.17-5             | 2:3.3.17-5             | 2:3.3.17-5             | 2:3.3.15-2             | 2:3.3.15-2             | 2:3.3.12-3+deb9u1     | 2:3.3.12-3+deb9u1     |
| [CacheTool]      | 9.0.3                  | 9.0.3                  | 8.4.1                  | 7.1.0                  | 7.1.0                  | 5.1.3                  | 4.1.0                  | 3.2.2                 | 3.2.2                 |
| [Ext IGBinary]   | 3.2.14                 | 3.2.14                 | 3.2.12                 | 3.2.7                  | 3.2.7                  | 3.2.7                  | 3.2.7                  | 3.2.7                 | 2.0.8                 |
| [Ext ZSTD]       | 0.12.3                 | 0.12.3                 | 0.12.0                 | 0.11.0                 | 0.11.0                 | 0.11.0                 | 0.11.0                 | 0.10.0                | 0.10.0                |
| [Ext Redis]      | 5.3.7                  | 5.3.7                  | 5.3.7                  | 5.3.7                  | 5.3.7                  | 5.3.7                  | 5.3.7                  | 4.3.0                 | 4.3.0                 |
| [Ext MemCache]   | 8.2                    | 8.2                    | 8.0                    | 4.0.5.2                | 4.0.5.2                | 4.0.5.2                | 4.0.5.2                | 4.0.5.2               | 3.0.8                 |
| [Composer]       | 2.5.8                  | 2.5.8                  | 2.4.4                  | 2.3.9                  | 2.3.9                  | 2.3.9                  | 2.2.16                 | 2.2.16                | 2.2.16                |

## Special directories

### /opt/docker/www

Copy `/opt/docker/www` to ${NGINX_SERVER_ROOT} value or default to `/var/www/html`

*Used for sync source directory to mounted volume.*

### /opt/docker/pre-start

Scripts directory lunched **before** start server.

### /opt/docker/post-start

Scripts directory lunched **after** start server.

### /opt/docker/config/nginx/conf.d

Adding specific configurations to Nginx (`/etc/nginx/nginx.conf`).

### /opt/docker/config/nginx/sites.d

Adding specific configurations to Nginx vHost (`/etc/nginx/sites-available/default.conf`).

#### Example

```
# /opt/docker/config/nginx/sites.d/error_pages.conf
error_page 404 /404.html;
```

### /opt/docker/config/php-fpm/conf.d

Adding specific configurations to PHP-FPM global (`/usr/local/etc/php-fpm.conf`).

### /opt/docker/config/php-fpm/pool.d

Adding specific configurations to PHP-FPM pool (`/usr/local/etc/php-fpm.d/www.conf`).

## Environment variables

### PHP

| Key                                       | Value                               | PHP Version |
|-------------------------------------------|-------------------------------------|-------------|
| PHP_ENGINE                                | "Off"                               | `any`       |
| PHP_SHORT_OPEN_TAG                        | "Off"                               | `any`       |
| PHP_PRECISION                             | "14"                                | `any`       |
| PHP_OUTPUT_BUFFERING                      | "4096"                              | `any`       |
| PHP_OUTPUT_HANDLER                        | ""                                  | `any`       |
| PHP_ZLIB_OUTPUT_COMPRESSION               | "Off"                               | `any`       |
| PHP_ZLIB_OUTPUT_COMPRESSION_LEVEL         | "-1"                                | `any`       |
| PHP_ZLIB_OUTPUT_HANDLER                   | ""                                  | `any`       |
| PHP_IMPLICIT_FLUSH                        | "Off"                               | `any`       |
| PHP_UNSERIALIZE_CALLBACK_FUNC             | ""                                  | `any`       |
| PHP_SERIALIZE_PRECISION                   | "-1"                                | `any`       |
| PHP_OPEN_BASEDIR                          | ""                                  | `any`       |
| PHP_DISABLE_FUNCTIONS                     | ""                                  | `any`       |
| PHP_DISABLE_CLASSES                       | ""                                  | `any`       |
| PHP_HIGHLIGHT_STRING                      | "#DD0000"                           | `any`       |
| PHP_HIGHLIGHT_COMMENT                     | "#FF8000"                           | `any`       |
| PHP_HIGHLIGHT_KEYWORD                     | "#007700"                           | `any`       |
| PHP_HIGHLIGHT_DEFAULT                     | "#0000BB"                           | `any`       |
| PHP_HIGHLIGHT_HTML                        | "#000000"                           | `any`       |
| PHP_IGNORE_USER_ABORT                     | "Off"                               | `any`       |
| PHP_REALPATH_CACHE_SIZE                   | "4M"                                | `any`       |
| PHP_REALPATH_CACHE_TTL                    | "120"                               | `any`       |
| PHP_ZEND_ENABLE_GC                        | "On"                                | `any`       |
| PHP_ZEND_MULTIBYTE                        | "Off"                               | `any`       |
| PHP_ZEND_SCRIPT_ENCODING                  | ""                                  | `any`       |
| PHP_ZEND_EXCEPTION_IGNORE_ARGS            | "On"                                | `>= 8.0.0`  |
| PHP_ZEND_EXCEPTION_STRING_PARAM_MAX_LEN   | "0"                                 | `>= 8.0.0`  |
| PHP_EXPOSE_PHP                            | "Off"                               | `any`       |
| PHP_MAX_EXECUTION_TIME                    | "30"                                | `any`       |
| PHP_MAX_INPUT_TIME                        | "60"                                | `any`       |
| PHP_MAX_INPUT_NESTING_LEVEL               | "64"                                | `any`       |
| PHP_MAX_INPUT_VARS                        | "1000"                              | `any`       |
| PHP_MEMORY_LIMIT                          | "128M"                              | `any`       |
| PHP_ERROR_REPORTING                       | "E_ALL & ~E_DEPRECATED & ~E_STRICT" | `any`       |
| PHP_DISPLAY_ERRORS                        | "Off"                               | `any`       |
| PHP_DISPLAY_STARTUP_ERRORS                | "Off"                               | `any`       |
| PHP_LOG_ERRORS                            | "On"                                | `any`       |
| PHP_LOG_ERRORS_MAX_LEN                    | "1024"                              | `any`       |
| PHP_IGNORE_REPEATED_ERRORS                | "Off"                               | `any`       |
| PHP_IGNORE_REPEATED_SOURCE                | "Off"                               | `any`       |
| PHP_REPORT_MEMLEAKS                       | "On"                                | `any`       |
| PHP_REPORT_ZEND_DEBUG                     | "1"                                 | `any`       |
| PHP_TRACK_ERRORS                          | "Off"                               | `any`       |
| PHP_TRACK_ERRORS                          | "Off"                               | `any`       |
| PHP_XMLRPC_ERRORS                         | "0"                                 | `any`       |
| PHP_XMLRPC_ERROR_NUMBER                   | "0"                                 | `any`       |
| PHP_HTML_ERRORS                           | "On"                                | `any`       |
| PHP_DOCREF_ROOT                           | ""                                  | `any`       |
| PHP_DOCREF_EXT                            | ""                                  | `any`       |
| PHP_ERROR_PREPEND_STRING                  | ""                                  | `any`       |
| PHP_ERROR_APPEND_STRING                   | ""                                  | `any`       |
| PHP_ERROR_LOG                             | ""                                  | `any`       |
| PHP_ARG_SEPARATOR_OUTPUT                  | "&"                                 | `any`       |
| PHP_ARG_SEPARATOR_INPUT                   | "&"                                 | `any`       |
| PHP_VARIABLES_ORDER                       | "GPCS"                              | `any`       |
| PHP_REQUEST_ORDER                         | "GP"                                | `any`       |
| PHP_REGISTER_ARGC_ARGV                    | "Off"                               | `any`       |
| PHP_AUTO_GLOBALS_JIT                      | "On"                                | `any`       |
| PHP_ENABLE_POST_DATA_READING              | "On"                                | `any`       |
| PHP_POST_MAX_SIZE                         | "8M"                                | `any`       |
| PHP_AUTO_PREPEND_FILE                     | ""                                  | `any`       |
| PHP_AUTO_APPEND_FILE                      | ""                                  | `any`       |
| PHP_DEFAULT_MIMETYPE                      | "text/html"                         | `any`       |
| PHP_DEFAULT_CHARSET                       | "UTF-8"                             | `any`       |
| PHP_INTERNAL_ENCODING                     | ""                                  | `any`       |
| PHP_INPUT_ENCODING                        | ""                                  | `any`       |
| PHP_OUTPUT_ENCODING                       | ""                                  | `any`       |
| PHP_DOC_ROOT                              | ""                                  | `any`       |
| PHP_USER_DIR                              | ""                                  | `any`       |
| PHP_SYS_TEMP_DIR                          | ""                                  | `any`       |
| PHP_ENABLE_DL                             | "Off"                               | `any`       |
| PHP_CGI_FORCE_REDIRECT                    | "1"                                 | `any`       |
| PHP_CGI_NPH                               | "0"                                 | `any`       |
| PHP_CGI_REDIRECT_STATUS_ENV               | ""                                  | `any`       |
| PHP_CGI_FIX_PATHINFO                      | "1"                                 | `any`       |
| PHP_CGI_DISCARD_PATH                      | "0"                                 | `any`       |
| PHP_FASTCGI_IMPERSONATE                   | "0"                                 | `any`       |
| PHP_FASTCGI_LOGGING                       | "1"                                 | `any`       |
| PHP_CGI_RFC2616_HEADERS                   | "0"                                 | `any`       |
| PHP_CGI_CHECK_SHEBANG_LINE                | "1"                                 | `any`       |
| PHP_FILE_UPLOADS                          | "On"                                | `any`       |
| PHP_UPLOAD_TMP_DIR                        | ""                                  | `any`       |
| PHP_UPLOAD_MAX_FILESIZE                   | "2M"                                | `any`       |
| PHP_MAX_FILE_UPLOADS                      | "20"                                | `any`       |
| PHP_ALLOW_URL_FOPEN                       | "On"                                | `any`       |
| PHP_ALLOW_URL_INCLUDE                     | "Off"                               | `any`       |
| PHP_FROM                                  | ""                                  | `any`       |
| PHP_USER_AGENT                            | ""                                  | `any`       |
| PHP_DEFAULT_SOCKET_TIMEOUT                | "60"                                | `any`       |
| PHP_AUTO_DETECT_LINE_ENDINGS              | "Off"                               | `any`       |
| PHP_CLI_SERVER_COLOR                      | "On"                                | `any`       |
| PHP_DATE_TIMEZONE                         | ""                                  | `any`       |
| PHP_DATE_DEFAULT_LATITUDE                 | "31.7667"                           | `any`       |
| PHP_DATE_DEFAULT_LONGITUDE                | "35.2333"                           | `any`       |
| PHP_DATE_SUNRISE_ZENITH                   | "90.583333"                         | `any`       |
| PHP_DATE_SUNSET_ZENITH                    | "90.583333"                         | `any`       |
| PHP_FILTER_DEFAULT                        | "unsafe_raw"                        | `any`       |
| PHP_FILTER_DEFAULT_FLAGS                  | ""                                  | `any`       |
| PHP_IMAP_ENABLE_INSECURE_RSH              | "0"                                 | `any`       |
| PHP_INTL_DEFAULT_LOCALE                   | ""                                  | `any`       |
| PHP_INTL_ERROR_LEVEL                      | "0"                                 | `any`       |
| PHP_INTL_USE_EXCEPTIONS                   | "0"                                 | `any`       |
| PHP_SQLITE3_EXTENSION_DIR                 | ""                                  | `any`       |
| PHP_SQLITE3_DEFENSIVE                     | "1"                                 | `any`       |
| PHP_PCRE_BACKTRACK_LIMIT                  | "1000000"                           | `any`       |
| PHP_PCRE_RECURSION_LIMIT                  | "100000"                            | `any`       |
| PHP_PCRE_JIT                              | "1"                                 | `any`       |
| PHP_PDO_ODBC_CONNECTION_POOLING           | "strict"                            | `any`       |
| PHP_PDO_MYSQL_CACHE_SIZE                  | "2000"                              | `any`       |
| PHP_PDO_MYSQL_DEFAULT_SOCKET              | ""                                  | `any`       |
| PHP_PHAR_READONLY                         | "On"                                | `any`       |
| PHP_PHAR_REQUIRE_HASH                     | "On"                                | `any`       |
| PHP_PHAR_CACHE_LIST                       | ""                                  | `any`       |
| PHP_MYSQL_ALLOW_LOCAL_INLINE              | "On"                                | `< 7.0.0`   |
| PHP_MYSQL_ALLOW_PERSISTENT                | "On"                                | `< 7.0.0`   |
| PHP_MYSQL_CACHE_SIZE                      | "2000"                              | `< 7.0.0`   |
| PHP_MYSQL_MAX_PERSISTENT                  | "-1"                                | `< 7.0.0`   |
| PHP_MYSQL_MAX_LINKS                       | "-1"                                | `< 7.0.0`   |
| PHP_MYSQL_DEFAULT_PORT                    | ""                                  | `< 7.0.0`   |
| PHP_MYSQL_DEFAULT_SOCKET                  | ""                                  | `< 7.0.0`   |
| PHP_MYSQL_DEFAULT_HOST                    | ""                                  | `< 7.0.0`   |
| PHP_MYSQL_DEFAULT_USER                    | ""                                  | `< 7.0.0`   |
| PHP_MYSQL_DEFAULT_PASSWORD                | ""                                  | `< 7.0.0`   |
| PHP_MYSQL_CONNECT_TIMEOUT                 | "60"                                | `< 7.0.0`   |
| PHP_MYSQL_TRACE_MODE                      | "Off"                               | `< 7.0.0`   |
| PHP_MYSQLI_MAX_PERSISTENT                 | "-1"                                | `any`       |
| PHP_MYSQLI_ALLOW_LOCAL_INLINE             | "Off"                               | `any`       |
| PHP_MYSQLI_ALLOW_PERSISTENT               | "On"                                | `any`       |
| PHP_MYSQLI_MAX_LINKS                      | "-1"                                | `any`       |
| PHP_MYSQLI_CACHE_SIZE                     | "2000"                              | `any`       |
| PHP_MYSQLI_DEFAULT_PORT                   | "3306"                              | `any`       |
| PHP_MYSQLI_DEFAULT_SOCKET                 | ""                                  | `any`       |
| PHP_MYSQLI_DEFAULT_HOST                   | ""                                  | `any`       |
| PHP_MYSQLI_DEFAULT_USER                   | ""                                  | `any`       |
| PHP_MYSQLI_DEFAULT_PW                     | ""                                  | `any`       |
| PHP_MYSQLI_RECONNECT                      | "Off"                               | `any`       |
| PHP_MYSQLND_COLLECT_STATISTICS            | "Off"                               | `any`       |
| PHP_MYSQLND_COLLECT_MEMORY_STATISTICS     | "Off"                               | `any`       |
| PHP_BCMATH_SCALE                          | "0"                                 | `any`       |
| PHP_BROWSCAP                              | ""                                  | `any`       |
| PHP_SESSION_SAVE_HANDLER                  | "files"                             | `any`       |
| PHP_SESSION_SAVE_PATH                     | ""                                  | `any`       |
| PHP_SESSION_USE_STRICT_MODE               | "0"                                 | `any`       |
| PHP_SESSION_USE_COOKIES                   | "1"                                 | `any`       |
| PHP_SESSION_COOKIE_SECURE                 | "0"                                 | `any`       |
| PHP_SESSION_USE_ONLY_COOKIES              | "1"                                 | `any`       |
| PHP_SESSION_NAME                          | "PHPSESSID"                         | `any`       |
| PHP_SESSION_AUTO_START                    | "0"                                 | `any`       |
| PHP_SESSION_COOKIE_LIFETIME               | "0"                                 | `any`       |
| PHP_SESSION_COOKIE_PATH                   | "/"                                 | `any`       |
| PHP_SESSION_COOKIE_DOMAIN                 | ""                                  | `any`       |
| PHP_SESSION_COOKIE_HTTPONLY               | ""                                  | `any`       |
| PHP_SESSION_SERIALIZE_HANDLER             | "php"                               | `any`       |
| PHP_SESSION_GC_PROBALITY                  | "1"                                 | `any`       |
| PHP_SESSION_GC_DIVISOR                    | "1000"                              | `any`       |
| PHP_SESSION_GC_MAXLIFETIME                | "1440"                              | `any`       |
| PHP_SESSION_REFERER_CHECK                 | ""                                  | `any`       |
| PHP_SESSION_CACHE_LIMITER                 | "nocache"                           | `any`       |
| PHP_SESSION_CACHE_EXPIRE                  | "180"                               | `any`       |
| PHP_SESSION_USE_TRANS_SID                 | "0"                                 | `any`       |
| PHP_SESSION_SID_LENGTH                    | "26"                                | `any`       |
| PHP_SESSION_TRANS_SID_TAGS                | "a=href,area=href,frame=src,form="  | `any`       |
| PHP_SESSION_TRANS_SID_HOSTS               | ""                                  | `any`       |
| PHP_SESSION_SID_BITS_PER_CHARACTER        | "5"                                 | `any`       |
| PHP_SESSION_UPLOAD_PROGRESS_ENABLED       | "On"                                | `any`       |
| PHP_SESSION_UPLOAD_PROGRESS_CLEANUP       | "On"                                | `any`       |
| PHP_SESSION_UPLOAD_PROGRESS_PREFIX        | "upload_progress_"                  | `any`       |
| PHP_SESSION_UPLOAD_PROGRESS_NAME          | "PHP_SESSION_UPLOAD_PROGRESS"       | `any`       |
| PHP_SESSION_UPLOAD_PROGRESS_FREQ          | "1%"                                | `any`       |
| PHP_SESSION_UPLOAD_PROGRESS_MIN_FREQ      | "1"                                 | `any`       |
| PHP_SESSION_LAZY_WRITE                    | "On"                                | `any`       |
| PHP_ZEND_ASSERTIONS                       | "-1"                                | `any`       |
| PHP_ASSERT_ACTIVE                         | "On"                                | `any`       |
| PHP_ASSERT_EXCEPTION                      | "Off"                               | `any`       |
| PHP_ASSERT_WARNING                        | "On"                                | `any`       |
| PHP_ASSERT_CALLBACK                       | ""                                  | `any`       |
| PHP_ASSERT_QUIET_EVAL                     | "0"                                 | `any`       |
| PHP_COM_TYPELIB_FILE                      | ""                                  | `any`       |
| PHP_COM_ALLOW_DCOM                        | "0"                                 | `any`       |
| PHP_COM_AUTOREGISTER_TYPELIB              | "0"                                 | `any`       |
| PHP_COM_AUTOREGISTER_CASESENSITIVE        | "1"                                 | `any`       |
| PHP_COM_AUTOREGISTER_VERBOSE              | "0"                                 | `any`       |
| PHP_COM_CODE_PAGE                         | ""                                  | `any`       |
| PHP_MBSTRING_LANGUAGE                     | "neutral"                           | `any`       |
| PHP_MBSTRING_INTERNAL_ENCODING            | ""                                  | `any`       |
| PHP_MBSTRING_HTTP_INPUT                   | "pass"                              | `any`       |
| PHP_MBSTRING_HTTP_OUTPUT                  | "pass"                              | `any`       |
| PHP_MBSTRING_ENCODING_TRANSLATION         | "Off"                               | `any`       |
| PHP_MBSTRING_DETECT_ORDER                 | ""                                  | `any`       |
| PHP_MBSTRING_SUBSTITUTE_CHARACTER         | ""                                  | `any`       |
| PHP_MBSTRING_FUNC_OVERLOAD                | "0"                                 | `any`       |
| PHP_MBSTRING_STRICT_DETECTION             | "On"                                | `any`       |
| PHP_MBSTRING_REGEX_STACK_LIMIT            | "100000"                            | `>= 7.4.0`  |
| PHP_MBSTRING_REGEX_RETRY_LIMIT            | "1000000"                           | `>= 7.4.0`  |
| PHP_GD_JPEG_IGNORE_WARNING                | "1"                                 | `any`       |
| PHP_EXIF_ENCODE_UNICODE                   | "ISO-8859-15"                       | `any`       |
| PHP_EXIF_DECODE_UNICODE_MOTOROLA          | "UCS-2BE"                           | `any`       |
| PHP_EXIF_DECODE_UNICODE_INTEL             | "UCS-2LE"                           | `any`       |
| PHP_EXIF_ENCODE_JIS                       | ""                                  | `any`       |
| PHP_EXIF_DECODE_JIS_MOTOROLA              | "JIS"                               | `any`       |
| PHP_EXIF_DECODE_JIS_INTEL                 | "JIS"                               | `any`       |
| PHP_TIDY_DEFAULT_CONFIG                   | ""                                  | `any`       |
| PHP_TIDY_CLEAN_OUTPUT                     | "Off"                               | `any`       |
| PHP_SOAP_WSDL_CACHE_ENABLED               | "1"                                 | `any`       |
| PHP_SOAP_WSDL_CACHE_DIR                   | "/tmp"                              | `any`       |
| PHP_SOAP_WSDL_CACHE_TTL                   | "86400"                             | `any`       |
| PHP_SOAP_WSDL_CACHE_LIMIT                 | "5"                                 | `any`       |
| PHP_SYSVSHM_INIT_MEM                      | "10000"                             | `any`       |
| PHP_LDAP_MAX_LINKS                        | "-1"                                | `any`       |
| PHP_DBA_DEFAULT_HANDLER                   | "DBA_DEFAULT"                       | `any`       |
| PHP_OPCACHE_ENABLE                        | "1"                                 | `any`       |
| PHP_OPCACHE_ENABLE_CLI                    | "0"                                 | `any`       |
| PHP_OPCACHE_MEMORY_CONSUMPTION            | "128"                               | `any`       |
| PHP_OPCACHE_INTERNED_STRING_BUFFER        | "8"                                 | `any`       |
| PHP_OPCACHE_MAX_ACCELERATED_FILES         | "10000"                             | `any`       |
| PHP_OPCACHE_MAX_WASTED_PERCENTAGE         | "5"                                 | `any`       |
| PHP_OPCACHE_USE_CWD                       | "1"                                 | `any`       |
| PHP_OPCACHE_VALIDATE_TIMESTAMPS           | "1"                                 | `any`       |
| PHP_OPCACHE_REVALIDATE_FREQ               | "2"                                 | `any`       |
| PHP_OPCACHE_REVALIDATE_PATH               | "0"                                 | `any`       |
| PHP_OPCACHE_SAVE_COMMENTS                 | "1"                                 | `any`       |
| PHP_OPCACHE_ENABLE_FILE_OVERRIDE          | "0"                                 | `any`       |
| PHP_OPCACHE_INHERITED_HACK                | "1"                                 | `any`       |
| PHP_OPCACHE_INHERITED_HACK                | "1"                                 | `any`       |
| PHP_OPCACHE_DUMP_FIX                      | "0"                                 | `any`       |
| PHP_OPCACHE_BLACKLIST_FILENAME            | ""                                  | `any`       |
| PHP_OPCACHE_MAX_FILE_SIZE                 | "0"                                 | `any`       |
| PHP_OPCACHE_CONSISTENCY_CHECKS            | "0"                                 | `any`       |
| PHP_OPCACHE_FORCE_RESTART_TIMEOUT         | "180"                               | `any`       |
| PHP_OPCACHE_ERROR_LOG                     | ""                                  | `any`       |
| PHP_OPCACHE_LOG_VERBOSITY_LEVEL           | "1"                                 | `any`       |
| PHP_OPCACHE_PREFERRED_MEMORY_MODEL        | ""                                  | `any`       |
| PHP_OPCACHE_PROTECT_MEMORY                | "0"                                 | `any`       |
| PHP_OPCACHE_RESTRICT_API                  | ""                                  | `any`       |
| PHP_OPCACHE_MMAP_BASE                     | ""                                  | `any`       |
| PHP_OPCACHE_FILE_CACHE_ONLY               | "0"                                 | `any`       |
| PHP_OPCACHE_FILE_CACHE_CONSISTENCY_CHECKS | "1"                                 | `any`       |
| PHP_OPCACHE_FILE_CACHE_FALLBACK           | "1"                                 | `any`       |
| PHP_OPCACHE_HUGE_CODE_PAGES               | "0"                                 | `any`       |
| PHP_OPCACHE_VALIDATE_PERMISSION           | "0"                                 | `any`       |
| PHP_OPCACHE_VALIDATE_ROOT                 | "0"                                 | `any`       |
| PHP_OPCACHE_OPT_DEBUG_LEVEL               | "0"                                 | `any`       |
| PHP_OPCACHE_PRELOAD                       | ""                                  | `any`       |
| PHP_OPCACHE_PRELOAD_USER                  | "www-data"                          | `any`       |
| PHP_CURL_CAINFO                           | ""                                  | `any`       |
| PHP_OPENSSL_CAFILE                        | ""                                  | `any`       |
| PHP_OPENSSL_CAPATH                        | ""                                  | `any`       |

### PHP-FPM

| Key                                  | Value                   | PHP Version |
|--------------------------------------|-------------------------|-------------|
| FPM_CONF_PID                         | ""                      | `any`       |
| FPM_CONF_ERROR_LOG                   | "/proc/self/fd/2"       | `any`       |
| FPM_CONF_SYSLOG_FACILITY             | "daemon"                | `any`       |
| FPM_CONF_SYSLOG_IDENT                | "php-fpm"               | `any`       |
| FPM_CONF_LOG_LEVEL                   | "notice"                | `any`       |
| FPM_CONF_EMERGENCY_RESTART_THRESHOLD | "0"                     | `any`       |
| FPM_CONF_EMERGENCY_RESTART_INTERVAL  | "0"                     | `any`       |
| FPM_CONF_PROCESS_CONTROL_TIMEOUT     | "0"                     | `any`       |
| FPM_CONF_PROCESS_MAX                 | "0"                     | `any`       |
| FPM_CONF_PROCESS_PRIORITY            | ""                      | `any`       |
| FPM_CONF_DAEMONIZE                   | "no"                    | `any`       |
| FPM_CONF_RLIMIT_FILES                | ""                      | `any`       |
| FPM_CONF_RLIMIT_C0RE                 | ""                      | `any`       |
| FPM_CONF_EVENTS_MECHANISM            | ""                      | `any`       |
| FPM_CONF_SYSTEMD_INTERVAL            | "10"                    | `any`       |
| FPM_WWW_PREFIX                       | ""                      | `any`       |
| FPM_WWW_USER                         | "www-data"              | `any`       |
| FPM_WWW_GROUP                        | "www-data"              | `any`       |
| FPM_WWW_LISTEN                       | "/run/php-fpm/www.sock" | `any`       |
| FPM_WWW_LISTEN_BACKLOG               | "511"                   | `any`       |
| FPM_WWW_LISTEN_OWNER                 | "www-data"              | `any`       |
| FPM_WWW_LISTEN_GROUP                 | "www-data"              | `any`       |
| FPM_WWW_LISTEN_MODE                  | "0660"                  | `any`       |
| FPM_WWW_LISTEN_ACL_USERS             | ""                      | `any`       |
| FPM_WWW_LISTEN_ACL_GROUPS            | ""                      | `any`       |
| FPM_WWW_LISTEN_ALLOWED_CLIENTS       | ""                      | `any`       |
| FPM_WWW_PROCESS_PRIORITY             | ""                      | `any`       |
| FPM_WWW_PROCESS_DUMPABLE             | "no"                    | `any`       |
| FPM_WWW_PM                           | "static"                | `any`       |
| FPM_WWW_PM_MAX_CHILDREN              | "5"                     | `any`       |
| FPM_WWW_PM_START_SERVERS             | "2"                     | `any`       |
| FPM_WWW_PM_MIN_SPARE_SERVERS         | "1"                     | `any`       |
| FPM_WWW_PM_MAX_SPARE_SERVERS         | "3"                     | `any`       |
| FPM_WWW_PM_PROCESS_IDLE_TIMEOUT      | "10s"                   | `any`       |
| FPM_WWW_PM_MAX_REQUESTS              | "500"                   | `any`       |
| FPM_WWW_PM_STATUS_PATH               | "/_fpm/status"          | `any`       |
| FPM_WWW_PING_PATH                    | "/_fpm/ping"            | `any`       |
| FPM_WWW_PING_RESPONSE                | "pong"                  | `any`       |
| FPM_WWW_ACCESS_LOG                   | "/proc/self/fd/2"       | `any`       |
| FPM_WWW_SLOWLOG                      | ""                      | `any`       |
| FPM_WWW_REQUEST_SLOWLOG_TIMEOUT      | "0"                     | `>= 7.2.5`  |
| FPM_WWW_REQUEST_SLOWLOG_TRACE_DEPTH  | "20"                    | `any`       |
| FPM_WWW_REQUEST_TERMINATE_TIMEOUT    | "0"                     | `any`       |
| FPM_WWW_RLIMIT_FILES                 | ""                      | `any`       |
| FPM_WWW_RLIMIT_CORE                  | ""                      | `any`       |
| FPM_WWW_CHROOT                       | ""                      | `any`       |
| FPM_WWW_CHDIR                        | ""                      | `any`       |
| FPM_WWW_CATCH_WORKERS_OUTPUT         | "yes"                   | `any`       |
| FPM_WWW_CLEAR_ENV                    | "no"                    | `any`       |
| FPM_WWW_SECURITY_LIMIT_EXTENSIONS    | ".php"                  | `any`       |

### Nginx

| Key                                       | Value                      |
|-------------------------------------------|----------------------------|
| NGNIX_CONF_USER                           | "www-data"                 |
| NGNIX_CONF_WORKER_PROCESSES               | "auto"                     |
| NGNIX_CONF_PID                            | "/run/nginx.pid"           |
| NGNIX_EVENT_WORKER_CONNECTIONS            | "1024"                     |
| NGNIX_HTTP_SENDFILE                       | "on"                       |
| NGNIX_HTTP_TCP_NOPUSH                     | "on"                       |
| NGNIX_HTTP_TCP_NODELAY                    | "on"                       |
| NGNIX_HTTP_KEEPALIVE_TIMEOUT              | "65"                       |
| NGNIX_HTTP_TYPES_HASH_MAX_SIZE            | "2048"                     |
| NGNIX_HTTP_SERVER_TOKENS                  | "off"                      |
| NGNIX_HTTP_SERVER_NAMES_HASH_BUCKET_SIZE  | "32"                       |
| NGNIX_HTTP_SERVER_NAME_IN_REDIRECT        | "off"                      |
| NGNIX_HTTP_DEFAULT_TYPE                   | "application/octet-stream" |
| NGNIX_HTTP_SSL_PROTOCOLS                  | "TLSv1 TLSv1.1 TLSv1.2"    |
| NGNIX_HTTP_SSL_PREFER_SERVER_CIPHERS      | "on"                       |
| NGNIX_HTTP_ACCESS_LOG                     | "/proc/self/fd/1"          |
| NGNIX_HTTP_ERROR_LOG                      | "/proc/self/fd/2"          |
| NGNIX_HTTP_GZIP                           | "on"                       |
| NGNIX_HTTP_GZIP_VARY                      | "on"                       |
| NGNIX_HTTP_GZIP_PROXIED                   | "any"                      |
| NGNIX_HTTP_GZIP_COMP_LEVEL                | "6"                        |
| NGNIX_HTTP_GZIP_BUFFERS                   | "16 8k"                    |
| NGNIX_HTTP_GZIP_HTTP_VERSION              | "1.1"                      |
| NGNIX_HTTP_GZIP_TYPES                     | "text/html"                |
| NGNIX_HTTP_GZIP_DISABLE                   | "msie6"                    |
| NGNIX_HTTP_GZIP_MIN_LENGTH                | "20"                       |
| NGINX_SERVER_ROOT                         | "/var/www/html"            |
| NGINX_SERVER_INDEX                        | "index.html"               |
| NGINX_SERVER_SERVER_NAME                  | "_"                        |
| NGINX_SERVER_CLIENT_MAX_BODY_SIZE         | "20m"                      |
| NGINX_SERVER_CLIENT_BODY_BUFFER_SIZE      | "128k"                     |
| NGINX_SERVER_REAL_IP_HEADER               | "X-Forwarded-For"          |
| NGINX_SERVER_REAL_IP_RECURSIVE            | "off"                      |
| NGINX_SERVER_SET_REAL_IP_FROM             | ""                         |
| NGINX_SERVER_FASTCGI_PARAMS_REMOTE_ADDR   | "$remote_addr"             |
| NGINX_SERVER_FASTCGI_BUFFERS              | "16 32k"                   |
| NGINX_SERVER_FASTCGI_BUFFER_SIZE          | "64k"                      |
| NGINX_SERVER_FASTCGI_BUSY_BUFFERS_SIZE    | "64k"                      |
| NGINX_SERVER_INTERNAL                     | "true"                     |
| NGINX_SERVER_OPTS_ROOT_LOCATION_TRY_FILES | ""                         |
| NGINX_SERVER_OPTS_INDEX_LOCATION          | ""                         |
| NGINX_SERVER_OPTS_ALLOW_PHP_FILES         | "false"                    |
| NGINX_SERVER_OPTS_ALLOW_HIDDEN_FILES      | "false"                    |
| NGINX_SERVER_OPTS_ALLOW_BUNDLE_FOLDER     | "false"                    |
| NGINX_SERVER_OPTS_STATUS_NGINX_PATH       | "/_status/nginx"           |
| NGINX_SERVER_OPTS_STATUS_NGINX_ALLOW      | "127.0.0.1"                |
| NGINX_SERVER_OPTS_STATUS_FPM_PATH         | "/_status/fpm"             |
| NGINX_SERVER_OPTS_STATUS_FPM_ALLOW        | "127.0.0.1"                |
| NGINX_SERVER_OPTS_STATUS_PING_PATH        | "/_status/ping"            |
| NGINX_SERVER_OPTS_STATUS_PING_ALLOW       | "127.0.0.1"                |
| NGINX_SERVER_OPTS_STATUS_PHP_PATH         | "/_status/php"             |
| NGINX_SERVER_OPTS_STATUS_PHP_ALLOW        | "127.0.0.1"                |
| NGINX_SERVER_OPTS_STATUS_OPCACHE_PATH     | "/_status/opcache"         |
| NGINX_SERVER_OPTS_STATUS_OPCACHE_ALLOW    | "127.0.0.1"                |
| NGINX_SERVER_OPTS_STATUS_METRICS_PATH     | "/_status/metrics"         |
| NGINX_SERVER_OPTS_STATUS_METRICS_ALLOW    | "127.0.0.1"                |

### Entrypoint

| Key                         | Value | Description                                                                |
|-----------------------------|-------|----------------------------------------------------------------------------|
| ENTRYPOINT_DEBUG            | false | Enabled debug mode on entrypoint                                           |
| ENTRYPOINT_EXTRA_FILES      | ""    | Forward custom log files into dir to stdout (full path separated with ';') |
| ENTRYPOINT_METRICS_ENABLED  | false | Enable OpenMetric/Prometheus metrics endpoint of                           |
| ENTRYPOINT_FPM_ARGS         | ""    | Add FPM command arguments                                                  |
| ENTRYPOINT_LOG_BUFFER_LIMIT | 1024  | Log buffer limit                                                           |


[CacheTool]: https://github.com/gordalina/cachetool
[Ext IGBinary]: https://pecl.php.net/package/igbinary
[Ext ZSTD]: https://pecl.php.net/package/zstd
[Ext Redis]: https://pecl.php.net/package/redis
[Ext MemCache]: https://pecl.php.net/package/memcache
[Composer]: https://github.com/composer/composer
