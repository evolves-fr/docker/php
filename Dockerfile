ARG GO_TAG
ARG COMPOSER_TAG
ARG PHP_TAG

################################################## Entrypoint Builder ##################################################
FROM golang:${GO_TAG} AS entrypoint
WORKDIR /go/src/gitlab.com/evolves-fr/docker/php
ARG VERSION
ARG REVISION
ENV GO111MODULE on
ENV CGO_ENABLED 1
COPY . .
RUN go mod download && go build -o /bin/entrypoint -ldflags="-X main.Version=${VERSION}-${REVISION}" .

##################################################      Composer      ##################################################
FROM composer:${COMPOSER_TAG} as composer

##################################################        Main        ##################################################
FROM php:${PHP_TAG}

########################################################################################################################
# Arguments
########################################################################################################################
ARG VERSION
ARG REVISION
ARG CREATED_AT
ARG CA_CERTIFICATES_VERSION
ARG LIBICU_DEV_VERSION
ARG LIBZIP_DEV_VERSION
ARG LIBZSTD_DEV_VERSION
ARG LIBGMP_DEV_VERSION
ARG LIBMEMCACHED_DEV_VERSION
ARG NGINX_VERSION
ARG PROCPS_VERSION
ARG CACHETOOL_VERSION
ARG EXT_IGBINARY_VERSION
ARG EXT_ZSTD_VERSION
ARG EXT_REDIS_VERSION
ARG EXT_MEMCACHE_VERSION
ARG EXT_MEMCACHE_CFLAG
ARG EXT_XDEBUG_VERSION

########################################################################################################################
# Labels
########################################################################################################################
LABEL maintainer="Evolves SAS <contact@evolves.fr>"
LABEL org.opencontainers.image.created="${CREATED_AT}"
LABEL org.opencontainers.image.authors="Evolves SAS <contact@evolves.fr>"
LABEL org.opencontainers.image.url="https://gitlab.com/evolves-fr/docker/php/container_registry"
LABEL org.opencontainers.image.documentation="https://gitlab.com/evolves-fr/docker/php/-/blob/main/README.md"
LABEL org.opencontainers.image.source="https://gitlab.com/evolves-fr/docker/php"
LABEL org.opencontainers.image.version="${VERSION}"
LABEL org.opencontainers.image.revision="${REVISION}"
LABEL org.opencontainers.image.vendor="Evolves SAS"
LABEL org.opencontainers.image.licenses="MIT"
LABEL org.opencontainers.image.ref.name="php"
LABEL org.opencontainers.image.title="PHP"
LABEL org.opencontainers.image.description="PHP and Nginx web server highly configurable"

########################################################################################################################
# Environments
########################################################################################################################
ENV PHP_ENGINE                                      "Off"
ENV PHP_SHORT_OPEN_TAG                              "Off"
ENV PHP_PRECISION                                   "14"
ENV PHP_OUTPUT_BUFFERING                            "4096"
ENV PHP_OUTPUT_HANDLER                              ""
ENV PHP_ZLIB_OUTPUT_COMPRESSION                     "Off"
ENV PHP_ZLIB_OUTPUT_COMPRESSION_LEVEL               "-1"
ENV PHP_ZLIB_OUTPUT_HANDLER                         ""
ENV PHP_IMPLICIT_FLUSH                              "Off"
ENV PHP_UNSERIALIZE_CALLBACK_FUNC                   ""
ENV PHP_SERIALIZE_PRECISION                         "-1"
ENV PHP_OPEN_BASEDIR                                ""
ENV PHP_DISABLE_FUNCTIONS                           ""
ENV PHP_DISABLE_CLASSES                             ""
ENV PHP_HIGHLIGHT_STRING                            "#DD0000"
ENV PHP_HIGHLIGHT_COMMENT                           "#FF8000"
ENV PHP_HIGHLIGHT_KEYWORD                           "#007700"
ENV PHP_HIGHLIGHT_DEFAULT                           "#0000BB"
ENV PHP_HIGHLIGHT_HTML                              "#000000"
ENV PHP_IGNORE_USER_ABORT                           "Off"
ENV PHP_REALPATH_CACHE_SIZE                         "4M"
ENV PHP_REALPATH_CACHE_TTL                          "120"
ENV PHP_ZEND_ENABLE_GC                              "On"
ENV PHP_ZEND_MULTIBYTE                              "Off"
ENV PHP_ZEND_SCRIPT_ENCODING                        ""
ENV PHP_ZEND_EXCEPTION_IGNORE_ARGS                  "On"
ENV PHP_ZEND_EXCEPTION_STRING_PARAM_MAX_LEN         "0"
ENV PHP_EXPOSE_PHP                                  "Off"
ENV PHP_MAX_EXECUTION_TIME                          "30"
ENV PHP_MAX_INPUT_TIME                              "60"
ENV PHP_MAX_INPUT_NESTING_LEVEL                     "64"
ENV PHP_MAX_INPUT_VARS                              "1000"
ENV PHP_MEMORY_LIMIT                                "128M"
ENV PHP_ERROR_REPORTING                             "E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED"
ENV PHP_DISPLAY_ERRORS                              "Off"
ENV PHP_DISPLAY_STARTUP_ERRORS                      "Off"
ENV PHP_LOG_ERRORS                                  "On"
ENV PHP_LOG_ERRORS_MAX_LEN                          "1024"
ENV PHP_IGNORE_REPEATED_ERRORS                      "Off"
ENV PHP_IGNORE_REPEATED_SOURCE                      "Off"
ENV PHP_REPORT_MEMLEAKS                             "On"
ENV PHP_REPORT_ZEND_DEBUG                           "1"
ENV PHP_TRACK_ERRORS                                "Off"
ENV PHP_TRACK_ERRORS                                "Off"
ENV PHP_XMLRPC_ERRORS                               "0"
ENV PHP_XMLRPC_ERROR_NUMBER                         "0"
ENV PHP_HTML_ERRORS                                 "On"
ENV PHP_DOCREF_ROOT                                 ""
ENV PHP_DOCREF_EXT                                  ""
ENV PHP_ERROR_PREPEND_STRING                        ""
ENV PHP_ERROR_APPEND_STRING                         ""
ENV PHP_ERROR_LOG                                   ""
ENV PHP_ARG_SEPARATOR_OUTPUT                        "&"
ENV PHP_ARG_SEPARATOR_INPUT                         "&"
ENV PHP_VARIABLES_ORDER                             "GPCS"
ENV PHP_REQUEST_ORDER                               "GP"
ENV PHP_REGISTER_ARGC_ARGV                          "Off"
ENV PHP_AUTO_GLOBALS_JIT                            "On"
ENV PHP_ENABLE_POST_DATA_READING                    "On"
ENV PHP_POST_MAX_SIZE                               "8M"
ENV PHP_AUTO_PREPEND_FILE                           ""
ENV PHP_AUTO_APPEND_FILE                            ""
ENV PHP_DEFAULT_MIMETYPE                            "text/html"
ENV PHP_DEFAULT_CHARSET                             "UTF-8"
ENV PHP_INTERNAL_ENCODING                           ""
ENV PHP_INPUT_ENCODING                              ""
ENV PHP_OUTPUT_ENCODING                             ""
ENV PHP_DOC_ROOT                                    ""
ENV PHP_USER_DIR                                    ""
ENV PHP_SYS_TEMP_DIR                                ""
ENV PHP_ENABLE_DL                                   "Off"
ENV PHP_CGI_FORCE_REDIRECT                          "1"
ENV PHP_CGI_NPH                                     "0"
ENV PHP_CGI_REDIRECT_STATUS_ENV                     ""
ENV PHP_CGI_FIX_PATHINFO                            "1"
ENV PHP_CGI_DISCARD_PATH                            "0"
ENV PHP_FASTCGI_IMPERSONATE                         "0"
ENV PHP_FASTCGI_LOGGING                             "1"
ENV PHP_CGI_RFC2616_HEADERS                         "0"
ENV PHP_CGI_CHECK_SHEBANG_LINE                      "1"
ENV PHP_FILE_UPLOADS                                "On"
ENV PHP_UPLOAD_TMP_DIR                              ""
ENV PHP_UPLOAD_MAX_FILESIZE                         "2M"
ENV PHP_MAX_FILE_UPLOADS                            "20"
ENV PHP_ALLOW_URL_FOPEN                             "On"
ENV PHP_ALLOW_URL_INCLUDE                           "Off"
ENV PHP_FROM                                        ""
ENV PHP_USER_AGENT                                  ""
ENV PHP_DEFAULT_SOCKET_TIMEOUT                      "60"
ENV PHP_AUTO_DETECT_LINE_ENDINGS                    "Off"
ENV PHP_CLI_SERVER_COLOR                            "On"
ENV PHP_DATE_TIMEZONE                               ""
ENV PHP_DATE_DEFAULT_LATITUDE                       "31.7667"
ENV PHP_DATE_DEFAULT_LONGITUDE                      "35.2333"
ENV PHP_DATE_SUNRISE_ZENITH                         "90.583333"
ENV PHP_DATE_SUNSET_ZENITH                          "90.583333"
ENV PHP_FILTER_DEFAULT                              "unsafe_raw"
ENV PHP_FILTER_DEFAULT_FLAGS                        ""
ENV PHP_IMAP_ENABLE_INSECURE_RSH                    "0"
ENV PHP_INTL_DEFAULT_LOCALE                         ""
ENV PHP_INTL_ERROR_LEVEL                            "0"
ENV PHP_INTL_USE_EXCEPTIONS                         "0"
ENV PHP_SQLITE3_EXTENSION_DIR                       ""
ENV PHP_SQLITE3_DEFENSIVE                           "1"
ENV PHP_PCRE_BACKTRACK_LIMIT                        "1000000"
ENV PHP_PCRE_RECURSION_LIMIT                        "100000"
ENV PHP_PCRE_JIT                                    "1"
ENV PHP_PDO_ODBC_CONNECTION_POOLING                 "strict"
ENV PHP_PDO_MYSQL_CACHE_SIZE                        "2000"
ENV PHP_PDO_MYSQL_DEFAULT_SOCKET                    ""
ENV PHP_PHAR_READONLY                               "On"
ENV PHP_PHAR_REQUIRE_HASH                           "On"
ENV PHP_PHAR_CACHE_LIST                             ""
ENV PHP_MYSQL_ALLOW_LOCAL_INLINE                    "On"
ENV PHP_MYSQL_ALLOW_PERSISTENT                      "On"
ENV PHP_MYSQL_CACHE_SIZE                            "2000"
ENV PHP_MYSQL_MAX_PERSISTENT                        "-1"
ENV PHP_MYSQL_MAX_LINKS                             "-1"
ENV PHP_MYSQL_DEFAULT_PORT                          ""
ENV PHP_MYSQL_DEFAULT_SOCKET                        ""
ENV PHP_MYSQL_DEFAULT_HOST                          ""
ENV PHP_MYSQL_DEFAULT_USER                          ""
ENV PHP_MYSQL_DEFAULT_PASSWORD                      ""
ENV PHP_MYSQL_CONNECT_TIMEOUT                       "60"
ENV PHP_MYSQL_TRACE_MODE                            "Off"
ENV PHP_MYSQLI_MAX_PERSISTENT                       "-1"
ENV PHP_MYSQLI_ALLOW_LOCAL_INLINE                   "Off"
ENV PHP_MYSQLI_ALLOW_PERSISTENT                     "On"
ENV PHP_MYSQLI_MAX_LINKS                            "-1"
ENV PHP_MYSQLI_CACHE_SIZE                           "2000"
ENV PHP_MYSQLI_DEFAULT_PORT                         "3306"
ENV PHP_MYSQLI_DEFAULT_SOCKET                       ""
ENV PHP_MYSQLI_DEFAULT_HOST                         ""
ENV PHP_MYSQLI_DEFAULT_USER                         ""
ENV PHP_MYSQLI_DEFAULT_PW                           ""
ENV PHP_MYSQLI_RECONNECT                            "Off"
ENV PHP_MYSQLND_COLLECT_STATISTICS                  "Off"
ENV PHP_MYSQLND_COLLECT_MEMORY_STATISTICS           "Off"
ENV PHP_BCMATH_SCALE                                "0"
ENV PHP_BROWSCAP                                    ""
ENV PHP_SESSION_SAVE_HANDLER                        "files"
ENV PHP_SESSION_SAVE_PATH                           ""
ENV PHP_SESSION_USE_STRICT_MODE                     "0"
ENV PHP_SESSION_USE_COOKIES                         "1"
ENV PHP_SESSION_COOKIE_SECURE                       "0"
ENV PHP_SESSION_USE_ONLY_COOKIES                    "1"
ENV PHP_SESSION_NAME                                "PHPSESSID"
ENV PHP_SESSION_AUTO_START                          "0"
ENV PHP_SESSION_COOKIE_LIFETIME                     "0"
ENV PHP_SESSION_COOKIE_PATH                         "/"
ENV PHP_SESSION_COOKIE_DOMAIN                       ""
ENV PHP_SESSION_COOKIE_HTTPONLY                     ""
ENV PHP_SESSION_SERIALIZE_HANDLER                   "php"
ENV PHP_SESSION_GC_PROBALITY                        "1"
ENV PHP_SESSION_GC_DIVISOR                          "1000"
ENV PHP_SESSION_GC_MAXLIFETIME                      "1440"
ENV PHP_SESSION_REFERER_CHECK                       ""
ENV PHP_SESSION_CACHE_LIMITER                       "nocache"
ENV PHP_SESSION_CACHE_EXPIRE                        "180"
ENV PHP_SESSION_USE_TRANS_SID                       "0"
ENV PHP_SESSION_SID_LENGTH                          "26"
ENV PHP_SESSION_TRANS_SID_TAGS                      "a=href,area=href,frame=src,form="
ENV PHP_SESSION_TRANS_SID_HOSTS                     ""
ENV PHP_SESSION_SID_BITS_PER_CHARACTER              "5"
ENV PHP_SESSION_UPLOAD_PROGRESS_ENABLED             "On"
ENV PHP_SESSION_UPLOAD_PROGRESS_CLEANUP             "On"
ENV PHP_SESSION_UPLOAD_PROGRESS_PREFIX              "upload_progress_"
ENV PHP_SESSION_UPLOAD_PROGRESS_NAME                "PHP_SESSION_UPLOAD_PROGRESS"
ENV PHP_SESSION_UPLOAD_PROGRESS_FREQ                "1%"
ENV PHP_SESSION_UPLOAD_PROGRESS_MIN_FREQ            "1"
ENV PHP_SESSION_LAZY_WRITE                          "On"
ENV PHP_ZEND_ASSERTIONS                             "-1"
ENV PHP_ASSERT_ACTIVE                               "On"
ENV PHP_ASSERT_EXCEPTION                            "Off"
ENV PHP_ASSERT_WARNING                              "On"
ENV PHP_ASSERT_CALLBACK                             ""
ENV PHP_ASSERT_QUIET_EVAL                           "0"
ENV PHP_COM_TYPELIB_FILE                            ""
ENV PHP_COM_ALLOW_DCOM                              "0"
ENV PHP_COM_AUTOREGISTER_TYPELIB                    "0"
ENV PHP_COM_AUTOREGISTER_CASESENSITIVE              "1"
ENV PHP_COM_AUTOREGISTER_VERBOSE                    "0"
ENV PHP_COM_CODE_PAGE                               ""
ENV PHP_MBSTRING_LANGUAGE                           "neutral"
ENV PHP_MBSTRING_INTERNAL_ENCODING                  ""
ENV PHP_MBSTRING_HTTP_INPUT                         "pass"
ENV PHP_MBSTRING_HTTP_OUTPUT                        "pass"
ENV PHP_MBSTRING_ENCODING_TRANSLATION               "Off"
ENV PHP_MBSTRING_DETECT_ORDER                       ""
ENV PHP_MBSTRING_SUBSTITUTE_CHARACTER               ""
ENV PHP_MBSTRING_FUNC_OVERLOAD                      "0"
ENV PHP_MBSTRING_STRICT_DETECTION                   "On"
ENV PHP_MBSTRING_REGEX_STACK_LIMIT                  "100000"
ENV PHP_MBSTRING_REGEX_RETRY_LIMIT                  "1000000"
ENV PHP_GD_JPEG_IGNORE_WARNING                      "1"
ENV PHP_EXIF_ENCODE_UNICODE                         "ISO-8859-15"
ENV PHP_EXIF_DECODE_UNICODE_MOTOROLA                "UCS-2BE"
ENV PHP_EXIF_DECODE_UNICODE_INTEL                   "UCS-2LE"
ENV PHP_EXIF_ENCODE_JIS                             ""
ENV PHP_EXIF_DECODE_JIS_MOTOROLA                    "JIS"
ENV PHP_EXIF_DECODE_JIS_INTEL                       "JIS"
ENV PHP_TIDY_DEFAULT_CONFIG                         ""
ENV PHP_TIDY_CLEAN_OUTPUT                           "Off"
ENV PHP_SOAP_WSDL_CACHE_ENABLED                     "1"
ENV PHP_SOAP_WSDL_CACHE_DIR                         "/tmp"
ENV PHP_SOAP_WSDL_CACHE_TTL                         "86400"
ENV PHP_SOAP_WSDL_CACHE_LIMIT                       "5"
ENV PHP_SYSVSHM_INIT_MEM                            "10000"
ENV PHP_LDAP_MAX_LINKS                              "-1"
ENV PHP_DBA_DEFAULT_HANDLER                         "DBA_DEFAULT"
ENV PHP_OPCACHE_ENABLE                              "1"
ENV PHP_OPCACHE_ENABLE_CLI                          "0"
ENV PHP_OPCACHE_MEMORY_CONSUMPTION                  "128"
ENV PHP_OPCACHE_INTERNED_STRING_BUFFER              "8"
ENV PHP_OPCACHE_MAX_ACCELERATED_FILES               "10000"
ENV PHP_OPCACHE_MAX_WASTED_PERCENTAGE               "5"
ENV PHP_OPCACHE_USE_CWD                             "1"
ENV PHP_OPCACHE_VALIDATE_TIMESTAMPS                 "1"
ENV PHP_OPCACHE_REVALIDATE_FREQ                     "2"
ENV PHP_OPCACHE_REVALIDATE_PATH                     "0"
ENV PHP_OPCACHE_SAVE_COMMENTS                       "1"
ENV PHP_OPCACHE_ENABLE_FILE_OVERRIDE                "0"
ENV PHP_OPCACHE_INHERITED_HACK                      "1"
ENV PHP_OPCACHE_INHERITED_HACK                      "1"
ENV PHP_OPCACHE_DUMP_FIX                            "0"
ENV PHP_OPCACHE_BLACKLIST_FILENAME                  ""
ENV PHP_OPCACHE_MAX_FILE_SIZE                       "0"
ENV PHP_OPCACHE_CONSISTENCY_CHECKS                  "0"
ENV PHP_OPCACHE_FORCE_RESTART_TIMEOUT               "180"
ENV PHP_OPCACHE_ERROR_LOG                           ""
ENV PHP_OPCACHE_LOG_VERBOSITY_LEVEL                 "1"
ENV PHP_OPCACHE_PREFERRED_MEMORY_MODEL              ""
ENV PHP_OPCACHE_PROTECT_MEMORY                      "0"
ENV PHP_OPCACHE_RESTRICT_API                        ""
ENV PHP_OPCACHE_MMAP_BASE                           ""
ENV PHP_OPCACHE_FILE_CACHE_ONLY                     "0"
ENV PHP_OPCACHE_FILE_CACHE_CONSISTENCY_CHECKS       "1"
ENV PHP_OPCACHE_FILE_CACHE_FALLBACK                 "1"
ENV PHP_OPCACHE_HUGE_CODE_PAGES                     "0"
ENV PHP_OPCACHE_VALIDATE_PERMISSION                 "0"
ENV PHP_OPCACHE_VALIDATE_ROOT                       "0"
ENV PHP_OPCACHE_OPT_DEBUG_LEVEL                     "0"
ENV PHP_OPCACHE_PRELOAD                             ""
ENV PHP_OPCACHE_PRELOAD_USER                        "www-data"
ENV PHP_CURL_CAINFO                                 ""
ENV PHP_OPENSSL_CAFILE                              ""
ENV PHP_OPENSSL_CAPATH                              ""

ENV FPM_CONF_PID                                    ""
ENV FPM_CONF_ERROR_LOG                              "/proc/self/fd/2"
ENV FPM_CONF_SYSLOG_FACILITY                        "daemon"
ENV FPM_CONF_SYSLOG_IDENT                           "php-fpm"
ENV FPM_CONF_LOG_LEVEL                              "warning"
ENV FPM_CONF_EMERGENCY_RESTART_THRESHOLD            "0"
ENV FPM_CONF_EMERGENCY_RESTART_INTERVAL             "0"
ENV FPM_CONF_PROCESS_CONTROL_TIMEOUT                "0"
ENV FPM_CONF_PROCESS_MAX                            "0"
ENV FPM_CONF_PROCESS_PRIORITY                       ""
ENV FPM_CONF_DAEMONIZE                              "no"
ENV FPM_CONF_RLIMIT_FILES                           ""
ENV FPM_CONF_RLIMIT_C0RE                            ""
ENV FPM_CONF_EVENTS_MECHANISM                       ""
ENV FPM_CONF_SYSTEMD_INTERVAL                       "10"
ENV FPM_WWW_PREFIX                                  ""
ENV FPM_WWW_USER                                    "www-data"
ENV FPM_WWW_GROUP                                   "www-data"
ENV FPM_WWW_LISTEN                                  "/run/php-fpm/www.sock"
ENV FPM_WWW_LISTEN_BACKLOG                          "511"
ENV FPM_WWW_LISTEN_OWNER                            "www-data"
ENV FPM_WWW_LISTEN_GROUP                            "www-data"
ENV FPM_WWW_LISTEN_MODE                             "0660"
ENV FPM_WWW_LISTEN_ACL_USERS                        ""
ENV FPM_WWW_LISTEN_ACL_GROUPS                       ""
ENV FPM_WWW_LISTEN_ALLOWED_CLIENTS                  ""
ENV FPM_WWW_PROCESS_PRIORITY                        ""
ENV FPM_WWW_PROCESS_DUMPABLE                        "no"
ENV FPM_WWW_PM                                      "static"
ENV FPM_WWW_PM_MAX_CHILDREN                         "5"
ENV FPM_WWW_PM_START_SERVERS                        "2"
ENV FPM_WWW_PM_MIN_SPARE_SERVERS                    "1"
ENV FPM_WWW_PM_MAX_SPARE_SERVERS                    "3"
ENV FPM_WWW_PM_PROCESS_IDLE_TIMEOUT                 "10s"
ENV FPM_WWW_PM_MAX_REQUESTS                         "500"
ENV FPM_WWW_PM_STATUS_PATH                          "/_fpm/status"
ENV FPM_WWW_PING_PATH                               "/_fpm/ping"
ENV FPM_WWW_PING_RESPONSE                           "pong"
ENV FPM_WWW_ACCESS_LOG                              "/proc/self/fd/3"
ENV FPM_WWW_SLOWLOG                                 ""
ENV FPM_WWW_REQUEST_SLOWLOG_TIMEOUT                 "0"
ENV FPM_WWW_REQUEST_SLOWLOG_TRACE_DEPTH             "20"
ENV FPM_WWW_REQUEST_TERMINATE_TIMEOUT               "0"
ENV FPM_WWW_RLIMIT_FILES                            ""
ENV FPM_WWW_RLIMIT_CORE                             ""
ENV FPM_WWW_CHROOT                                  ""
ENV FPM_WWW_CHDIR                                   ""
ENV FPM_WWW_CATCH_WORKERS_OUTPUT                    "yes"
ENV FPM_WWW_CLEAR_ENV                               "no"
ENV FPM_WWW_SECURITY_LIMIT_EXTENSIONS               ".php"

ENV NGNIX_CONF_USER                                 "www-data"
ENV NGNIX_CONF_WORKER_PROCESSES                     "auto"
ENV NGNIX_CONF_PID                                  "/run/nginx.pid"
ENV NGNIX_EVENT_WORKER_CONNECTIONS                  "1024"
ENV NGNIX_HTTP_SENDFILE                             "on"
ENV NGNIX_HTTP_TCP_NOPUSH                           "on"
ENV NGNIX_HTTP_TCP_NODELAY                          "on"
ENV NGNIX_HTTP_KEEPALIVE_TIMEOUT                    "65"
ENV NGNIX_HTTP_TYPES_HASH_MAX_SIZE                  "2048"
ENV NGNIX_HTTP_SERVER_TOKENS                        "off"
ENV NGNIX_HTTP_SERVER_NAMES_HASH_BUCKET_SIZE        "32"
ENV NGNIX_HTTP_SERVER_NAME_IN_REDIRECT              "off"
ENV NGNIX_HTTP_DEFAULT_TYPE                         "application/octet-stream"
ENV NGNIX_HTTP_SSL_PROTOCOLS                        "TLSv1 TLSv1.1 TLSv1.2"
ENV NGNIX_HTTP_SSL_PREFER_SERVER_CIPHERS            "on"
ENV NGNIX_HTTP_ACCESS_LOG                           "/proc/self/fd/1"
ENV NGNIX_HTTP_ERROR_LOG                            "/proc/self/fd/2"
ENV NGNIX_HTTP_GZIP                                 "on"
ENV NGNIX_HTTP_GZIP_VARY                            "on"
ENV NGNIX_HTTP_GZIP_PROXIED                         "any"
ENV NGNIX_HTTP_GZIP_COMP_LEVEL                      "6"
ENV NGNIX_HTTP_GZIP_BUFFERS                         "16 8k"
ENV NGNIX_HTTP_GZIP_HTTP_VERSION                    "1.1"
ENV NGNIX_HTTP_GZIP_TYPES                           "text/plain text/css application/json application/javascript text/xml application/xml application/xml+rss text/javascript"
ENV NGNIX_HTTP_GZIP_DISABLE                         "msie6"
ENV NGNIX_HTTP_GZIP_MIN_LENGTH                      "20"

ENV NGINX_SERVER_ROOT                               ""
ENV NGINX_SERVER_INDEX                              "index.php index.html"
ENV NGINX_SERVER_SERVER_NAME                        "_"
ENV NGINX_SERVER_CLIENT_MAX_BODY_SIZE               "20m"
ENV NGINX_SERVER_CLIENT_BODY_BUFFER_SIZE            "128k"
ENV NGINX_SERVER_REAL_IP_HEADER                     "X-Forwarded-For"
ENV NGINX_SERVER_REAL_IP_RECURSIVE                  "off"
ENV NGINX_SERVER_SET_REAL_IP_FROM                   ""
ENV NGINX_SERVER_FASTCGI_PARAMS_REMOTE_ADDR         "\$remote_addr"
ENV NGINX_SERVER_FASTCGI_BUFFERS                    "16 32k"
ENV NGINX_SERVER_FASTCGI_BUFFER_SIZE                "64k"
ENV NGINX_SERVER_FASTCGI_BUSY_BUFFERS_SIZE          "64k"
ENV NGINX_SERVER_FASTCGI_READ_TIMEOUT               "60s"
ENV NGINX_SERVER_INTERNAL                           "true"

ENV NGINX_SERVER_OPTS_ROOT_LOCATION_TRY_FILES       ""
ENV NGINX_SERVER_OPTS_INDEX_LOCATION                ""
ENV NGINX_SERVER_OPTS_ALLOW_PHP_FILES               "false"
ENV NGINX_SERVER_OPTS_ALLOW_HIDDEN_FILES            "false"
ENV NGINX_SERVER_OPTS_ALLOW_BUNDLE_FOLDER           "false"
ENV NGINX_SERVER_OPTS_STATUS_NGINX_PATH             "/_status/nginx"
ENV NGINX_SERVER_OPTS_STATUS_NGINX_ALLOW            "127.0.0.1"
ENV NGINX_SERVER_OPTS_STATUS_FPM_PATH               "/_status/fpm"
ENV NGINX_SERVER_OPTS_STATUS_FPM_ALLOW              "127.0.0.1"
ENV NGINX_SERVER_OPTS_STATUS_PING_PATH              "/_status/ping"
ENV NGINX_SERVER_OPTS_STATUS_PING_ALLOW             "127.0.0.1"
ENV NGINX_SERVER_OPTS_STATUS_PHP_PATH               "/_status/php"
ENV NGINX_SERVER_OPTS_STATUS_PHP_ALLOW              "127.0.0.1"
ENV NGINX_SERVER_OPTS_STATUS_OPCACHE_PATH           "/_status/opcache"
ENV NGINX_SERVER_OPTS_STATUS_OPCACHE_ALLOW          "127.0.0.1"
ENV NGINX_SERVER_OPTS_STATUS_METRICS_PATH           "/_status/metrics"
ENV NGINX_SERVER_OPTS_STATUS_METRICS_ALLOW          "127.0.0.1"

ENV ENTRYPOINT_DEBUG                                "false"
ENV ENTRYPOINT_EXTRA_FILES                          ""
ENV ENTRYPOINT_METRICS_ENABLED                      "false"
ENV ENTRYPOINT_FPM_ARGS                             ""
ENV ENTRYPOINT_LOG_BUFFER_LIMIT                     "1024"

########################################################################################################################
# Declarations
########################################################################################################################
ENTRYPOINT                                          ["/bin/entrypoint"]
CMD                                                 ["serve"]
WORKDIR                                             /var/www/html
EXPOSE                                              80

########################################################################################################################
# Install dependencies
########################################################################################################################
RUN apt update && apt upgrade -y && apt install -y \
    ca-certificates=${CA_CERTIFICATES_VERSION} \
    libicu-dev=${LIBICU_DEV_VERSION} \
    libzip-dev=${LIBZIP_DEV_VERSION} \
    libzstd-dev=${LIBZSTD_DEV_VERSION} \
    libgmp-dev=${LIBGMP_DEV_VERSION} \
    libmemcached-dev=${LIBMEMCACHED_DEV_VERSION} \
    nginx=${NGINX_VERSION} \
    procps=${PROCPS_VERSION} \
    $PHPIZE_DEPS gcc make autoconf \
    && rm -rf /var/lib/apt/lists/*

########################################################################################################################
# Install cachetool
########################################################################################################################
ADD https://gordalina.github.io/cachetool/downloads/cachetool-${CACHETOOL_VERSION}.phar /usr/local/bin/cachetool
RUN chmod a+x /usr/local/bin/cachetool

########################################################################################################################
# Install PHP extensions
########################################################################################################################
RUN ln -s /usr/include/x86_64-linux-gnu/gmp.h /usr/include/gmp.h
RUN ln -s /usr/local/etc/php/php.ini-production /usr/local/etc/php/php.ini
RUN pecl install \
    igbinary-${EXT_IGBINARY_VERSION} \
    zstd-${EXT_ZSTD_VERSION} \
    xdebug-${EXT_XDEBUG_VERSION} && \
    printf "yes\nyes\nyes\n" | pecl install redis-${EXT_REDIS_VERSION} && \
    printf "yes\n" | CFLAGS="${EXT_MEMCACHE_CFLAG}" pecl install memcache-${EXT_MEMCACHE_VERSION} && \
    docker-php-ext-configure intl && \
    docker-php-ext-install pdo pdo_mysql mysqli intl zip opcache gmp && \
    docker-php-ext-enable pdo_mysql intl opcache igbinary zstd redis memcache gmp

########################################################################################################################
# Install Composer
########################################################################################################################
COPY --from=composer /usr/bin/composer /usr/local/bin/composer

########################################################################################################################
# Clean
########################################################################################################################
RUN apt remove -y --purge $PHPIZE_DEPS gcc make autoconf && \
    apt -y autoremove --purge && \
    apt clean && \
    rm -rf /var/lib/apt/lists/* \
    /var/www/html/* \
    /usr/local/etc/php-fpm.d/* \
    /etc/nginx/sites-available/* \
    /etc/nginx/sites-enabled/*

########################################################################################################################
# Create and copy directory structure
########################################################################################################################
RUN mkdir -p \
    /opt/docker/config/nginx/conf.d \
    /opt/docker/config/nginx/sites.d \
    /opt/docker/config/php-fpm/conf.d \
    /opt/docker/config/php-fpm/pool.d \
    /opt/docker/pre-start \
    /opt/docker/post-start \
    /opt/docker/www \
    /var/log/php-fpm
ADD rootfs /
COPY --from=entrypoint /bin/entrypoint /bin/entrypoint
RUN chown -R www-data:www-data /var/www/html
RUN ln -s /etc/nginx/sites-available/default.conf /etc/nginx/sites-enabled/default.conf
