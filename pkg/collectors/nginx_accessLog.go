package collectors

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io"
	"strconv"
	"strings"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/sirupsen/logrus"
)

func NginxAccessLog(namespace string, reader io.Reader) prometheus.Collector {
	collector := prometheus.NewHistogramVec(prometheus.HistogramOpts{
		Namespace: namespace,
		Subsystem: "http",
		Name:      "request_duration_seconds",
		Help:      "request duration histogram",
		Buckets:   []float64{0.2, 0.5, 1, 2, 5},
	}, []string{"method", "status"})

	go func() {
		scanner := bufio.NewScanner(reader)
		if err := scanner.Err(); err != nil && err != io.EOF {
			return
		}

		for scanner.Scan() {
			var accessLog nginxAccessLog

			if err := json.Unmarshal(scanner.Bytes(), &accessLog); err != nil {
				continue
			}

			collector.With(prometheus.Labels{
				"method": accessLog.Method,
				"status": strconv.FormatUint(uint64(accessLog.Status), 10),
			}).Observe(accessLog.Latency.Seconds())

			logrus.WithFields(logrus.Fields{
				"process":     "nginx",
				"method":      accessLog.Method,
				"url":         accessLog.URL,
				"status":      accessLog.Status,
				"latency":     accessLog.Latency,
				"remote_ip":   accessLog.RemoteIP,
				"remote_addr": accessLog.RemoteAddr,
				"bytes_in":    accessLog.BytesIn,
				"bytes_out":   accessLog.BytesOut,
				"user_agent":  accessLog.UserAgent,
				"referrer":    accessLog.Referrer,
				"id":          accessLog.ID,
			}).Log(logrus.InfoLevel)
		}
	}()

	return collector
}

type nginxAccessLog struct {
	Method     string           `json:"method"`
	URL        string           `json:"url"`
	Status     uint16           `json:"status"`
	Latency    nginxRequestTime `json:"latency"`
	RemoteIP   string           `json:"remote_ip"`
	RemoteAddr string           `json:"remote_addr"`
	BytesIn    uint64           `json:"bytes_in"`
	BytesOut   uint64           `json:"bytes_out"`
	UserAgent  string           `json:"user_agent"`
	Referrer   string           `json:"referrer"`
	ID         string           `json:"id"`
}

type nginxRequestTime struct {
	time.Duration
}

func (t *nginxRequestTime) UnmarshalJSON(v []byte) error {
	var err error
	t.Duration, err = time.ParseDuration(strings.Trim(string(v), `"`) + "s")
	return err
}

func (t *nginxRequestTime) MarshalJSON() ([]byte, error) {
	return []byte(fmt.Sprintf("%.3f", t.Seconds())), nil
}
