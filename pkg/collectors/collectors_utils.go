package collectors

import (
	"bytes"
	"encoding/binary"
	"errors"
	"io"
	"net"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/prometheus/client_golang/prometheus"
)

func do(method string, url string, header http.Header, body io.Reader) (*http.Response, error) {
	// Initialize HTTP dialer with timeout
	dialer := &net.Dialer{
		Timeout:   time.Second * 5,
		KeepAlive: time.Second * 5,
	}

	// Initialize HTTP client with timeout
	client := &http.Client{
		Transport: &http.Transport{
			Proxy:                 http.ProxyFromEnvironment,
			DialContext:           dialer.DialContext,
			TLSHandshakeTimeout:   time.Second * 5,
			MaxIdleConns:          5,
			MaxIdleConnsPerHost:   5,
			MaxConnsPerHost:       5,
			IdleConnTimeout:       time.Second * 5,
			ExpectContinueTimeout: time.Second * 1,
			ForceAttemptHTTP2:     false,
			DisableKeepAlives:     false,
			DisableCompression:    false,
		},
		Timeout: time.Second * 5,
	}
	defer client.CloseIdleConnections()

	// Generate HTTP request
	req, err := http.NewRequest(method, url, body)
	if err != nil {
		return nil, err
	}

	// Set request header
	req.Header = header

	// Send HTTP request
	res, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	// Check response status code
	if res.StatusCode >= http.StatusBadRequest {
		return nil, errors.New(http.StatusText(res.StatusCode))
	}

	// Read body response
	return res, err
}

func counter(desc *prometheus.Desc, value float64, labelValues ...string) prometheus.Metric {
	return prometheus.MustNewConstMetric(desc, prometheus.CounterValue, value, labelValues...)
}

func gauge(desc *prometheus.Desc, value float64, labelValues ...string) prometheus.Metric {
	return prometheus.MustNewConstMetric(desc, prometheus.GaugeValue, value, labelValues...)
}

func truncate(s string, l int) string {
	if len(s) < l {
		return s
	}

	return strings.TrimSpace(s[:strings.LastIndex(s[:l-1], " ")])
}

type Unix int64

func (ts Unix) Time() time.Time {
	return time.Unix(int64(ts), 0)
}

func (ts Unix) String() string {
	return strconv.FormatInt(int64(ts), 10)
}

func (ts Unix) MarshalBinary() ([]byte, error) {
	buffer := new(bytes.Buffer)
	_ = binary.Write(buffer, binary.BigEndian, ts)

	return buffer.Bytes(), nil
}

func (ts *Unix) UnmarshalBinary(data []byte) error {
	var value int64
	if err := binary.Read(bytes.NewReader(data), binary.BigEndian, &value); err != nil {
		return err
	}

	*ts = Unix(value)

	return nil
}
