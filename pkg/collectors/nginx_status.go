package collectors

import (
	"net/http"
	"os"
	"regexp"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/sirupsen/logrus"
	"gitlab.com/evolves-fr/gommon"
	"gitlab.com/evolves-fr/gommon/encoding"
	"gopkg.org/generic"
	"gopkg.org/header"
)

func NginxStatus(namespace string) prometheus.Collector {
	return &nginxStatusCollector{
		actives: prometheus.NewDesc(
			prometheus.BuildFQName(namespace, "nginx", "actives"),
			"The current number of active client connections including Waiting connections.",
			nil,
			nil,
		),
		accepts: prometheus.NewDesc(
			prometheus.BuildFQName(namespace, "nginx", "accepts"),
			"The total number of accepted client connections.",
			nil,
			nil,
		),
		handled: prometheus.NewDesc(
			prometheus.BuildFQName(namespace, "nginx", "handled"),
			"The total number of handled connections.",
			nil,
			nil,
		),
		requests: prometheus.NewDesc(
			prometheus.BuildFQName(namespace, "nginx", "requests"),
			"The total number of client requests.",
			nil,
			nil,
		),
		reading: prometheus.NewDesc(
			prometheus.BuildFQName(namespace, "nginx", "reading"),
			"The current number of connections where nginx is reading the request header.",
			nil,
			nil,
		),
		writing: prometheus.NewDesc(
			prometheus.BuildFQName(namespace, "nginx", "writing"),
			"The current number of connections where nginx is writing the response back to the client.",
			nil,
			nil,
		),
		waiting: prometheus.NewDesc(
			prometheus.BuildFQName(namespace, "nginx", "waiting"),
			"The current number of idle client connections waiting for a request.",
			nil,
			nil,
		),
	}
}

type nginxStatusCollector struct {
	actives  *prometheus.Desc
	accepts  *prometheus.Desc
	handled  *prometheus.Desc
	requests *prometheus.Desc
	reading  *prometheus.Desc
	writing  *prometheus.Desc
	waiting  *prometheus.Desc
}

func (c *nginxStatusCollector) Describe(ch chan<- *prometheus.Desc) {
	ch <- c.actives
	ch <- c.accepts
	ch <- c.handled
	ch <- c.requests
	ch <- c.reading
	ch <- c.writing
	ch <- c.waiting
}

func (c *nginxStatusCollector) Collect(ch chan<- prometheus.Metric) {
	if res, err := c.collect(); err == nil {
		ch <- counter(c.actives, float64(res.Actives))
		ch <- gauge(c.accepts, float64(res.Accepts))
		ch <- counter(c.handled, float64(res.Handled))
		ch <- gauge(c.requests, float64(res.Requests))
		ch <- counter(c.reading, float64(res.Reading))
		ch <- gauge(c.writing, float64(res.Writing))
		ch <- gauge(c.waiting, float64(res.Waiting))
	} else {
		logrus.WithField("collector", "nginx_status").Error(err)
	}
}

func (c *nginxStatusCollector) collect() (nginxStatus, error) {
	url := "http://127.0.0.1" + generic.Default(os.Getenv("NGINX_SERVER_OPTS_STATUS_NGINX_PATH"), "/_status/nginx")

	res, err := do(http.MethodGet, url, http.Header{header.UserAgent: []string{"docker-php"}}, nil)
	if err != nil {
		return nginxStatus{}, err
	}
	defer res.Body.Close()

	var status nginxStatus

	if err = encoding.NewDecoder(res.Body).Decode(&status); err != nil {
		return nginxStatus{}, err
	}

	return status, nil
}

type nginxStatus struct {
	// The current number of active client connections including Waiting connections.
	Actives uint64

	// The total number of accepted client connections.
	Accepts uint64

	// The total number of handled connections. Generally, the parameter value is the same as accepts unless some
	// resource limits have been reached (for example, the worker_connections limit).
	Handled uint64

	// The total number of client requests.
	Requests uint64

	// The current number of connections where nginx is reading the request header.
	Reading uint64

	// The current number of connections where nginx is writing the response back to the client.
	Writing uint64

	// The current number of idle client connections waiting for a request.
	Waiting uint64
}

func (s *nginxStatus) UnmarshalText(text []byte) error {
	rxp := regexp.MustCompile(`^Active\sconnections:\s(?P<actives>\d+)\s?
server\saccepts\shandled\srequests
\s?(?P<accepts>\d+)\s(?P<handled>\d+)\s(?P<requests>\d+)\s?
Reading:\s(?P<reading>\d+)\sWriting:\s(?P<writing>\d+)\sWaiting:\s(?P<waiting>\d+)\s?\n?$`)

	match := rxp.FindStringSubmatch(string(text))

	if len(match) > 0 {
		s.Actives = gommon.MustParse[uint64](match[rxp.SubexpIndex("actives")])
		s.Accepts = gommon.MustParse[uint64](match[rxp.SubexpIndex("accepts")])
		s.Handled = gommon.MustParse[uint64](match[rxp.SubexpIndex("handled")])
		s.Requests = gommon.MustParse[uint64](match[rxp.SubexpIndex("requests")])
		s.Reading = gommon.MustParse[uint64](match[rxp.SubexpIndex("reading")])
		s.Writing = gommon.MustParse[uint64](match[rxp.SubexpIndex("writing")])
		s.Waiting = gommon.MustParse[uint64](match[rxp.SubexpIndex("waiting")])
	}

	return nil
}
