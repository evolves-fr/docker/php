package collectors

import (
	"encoding/json"
	"github.com/sirupsen/logrus"
	"net/http"
	"os"

	"github.com/prometheus/client_golang/prometheus"
	"gopkg.org/generic"
	"gopkg.org/header"
)

func OPCacheStatus(namespace string) prometheus.Collector {
	return &opcacheCollector{
		enabled: prometheus.NewDesc(
			prometheus.BuildFQName(namespace, "opcache", "enabled"),
			"",
			nil,
			nil,
		),
		cacheFull: prometheus.NewDesc(
			prometheus.BuildFQName(namespace, "opcache", "cache_full"),
			"",
			nil,
			nil,
		),
		restartPending: prometheus.NewDesc(
			prometheus.BuildFQName(namespace, "opcache", "restart_pending"),
			"",
			nil,
			nil,
		),
		restartInProgress: prometheus.NewDesc(
			prometheus.BuildFQName(namespace, "opcache", "restart_in_progress"),
			"",
			nil,
			nil,
		),
		memoryUsageUsedMemory: prometheus.NewDesc(
			prometheus.BuildFQName(namespace, "opcache", "memory_usage_used_memory"),
			"",
			nil,
			nil,
		),
		memoryUsageFreeMemory: prometheus.NewDesc(
			prometheus.BuildFQName(namespace, "opcache", "memory_usage_free_memory"),
			"",
			nil,
			nil,
		),
		memoryUsageWastedMemory: prometheus.NewDesc(
			prometheus.BuildFQName(namespace, "opcache", "memory_usage_wasted_memory"),
			"",
			nil,
			nil,
		),
		memoryUsageCurrentWastedPercentage: prometheus.NewDesc(
			prometheus.BuildFQName(namespace, "opcache", "memory_usage_current_wasted_percentage"),
			"",
			nil,
			nil,
		),
		internedStringsUsageBufferSize: prometheus.NewDesc(
			prometheus.BuildFQName(namespace, "opcache", "interned_strings_usage_buffer_size"),
			"",
			nil,
			nil,
		),
		internedStringsUsageUsedMemory: prometheus.NewDesc(
			prometheus.BuildFQName(namespace, "opcache", "interned_strings_usage_used_memory"),
			"",
			nil,
			nil,
		),
		internedStringsUsageFreeMemory: prometheus.NewDesc(
			prometheus.BuildFQName(namespace, "opcache", "interned_strings_usage_free_memory"),
			"",
			nil,
			nil,
		),
		internedStringsUsageNumberOfStrings: prometheus.NewDesc(
			prometheus.BuildFQName(namespace, "opcache", "interned_strings_usage_number_of_strings"),
			"",
			nil,
			nil,
		),
		statisticsNumCachedScripts: prometheus.NewDesc(
			prometheus.BuildFQName(namespace, "opcache", "num_cached_scripts"),
			"",
			nil,
			nil,
		),
		statisticsNumCachedKeys: prometheus.NewDesc(
			prometheus.BuildFQName(namespace, "opcache", "num_cached_keys"),
			"",
			nil,
			nil,
		),
		statisticsMaxCachedKeys: prometheus.NewDesc(
			prometheus.BuildFQName(namespace, "opcache", "max_cached_keys"),
			"",
			nil,
			nil,
		),
		statisticsHits: prometheus.NewDesc(
			prometheus.BuildFQName(namespace, "opcache", "hits"),
			"",
			nil,
			nil,
		),
		statisticsStartTime: prometheus.NewDesc(
			prometheus.BuildFQName(namespace, "opcache", "start_time"),
			"",
			nil,
			nil,
		),
		statisticsLastRestartTime: prometheus.NewDesc(
			prometheus.BuildFQName(namespace, "opcache", "last_restart_time"),
			"",
			nil,
			nil,
		),
		statisticsOomRestarts: prometheus.NewDesc(
			prometheus.BuildFQName(namespace, "opcache", "oom_restarts"),
			"",
			nil,
			nil,
		),
		statisticsHashRestarts: prometheus.NewDesc(
			prometheus.BuildFQName(namespace, "opcache", "hash_restarts"),
			"",
			nil,
			nil,
		),
		statisticsManualRestarts: prometheus.NewDesc(
			prometheus.BuildFQName(namespace, "opcache", "manual_restarts"),
			"",
			nil,
			nil,
		),
		statisticsMisses: prometheus.NewDesc(
			prometheus.BuildFQName(namespace, "opcache", "misses"),
			"",
			nil,
			nil,
		),
		statisticsBlacklistMisses: prometheus.NewDesc(
			prometheus.BuildFQName(namespace, "opcache", "blacklist_misses"),
			"",
			nil,
			nil,
		),
		statisticsBlacklistMissRatio: prometheus.NewDesc(
			prometheus.BuildFQName(namespace, "opcache", "blacklist_miss_ratio"),
			"",
			nil,
			nil,
		),
		statisticsHitRate: prometheus.NewDesc(
			prometheus.BuildFQName(namespace, "opcache", "hit_rate"),
			"",
			nil,
			nil,
		),
	}
}

type opcacheCollector struct {
	enabled                             *prometheus.Desc
	cacheFull                           *prometheus.Desc
	restartPending                      *prometheus.Desc
	restartInProgress                   *prometheus.Desc
	memoryUsageUsedMemory               *prometheus.Desc
	memoryUsageFreeMemory               *prometheus.Desc
	memoryUsageWastedMemory             *prometheus.Desc
	memoryUsageCurrentWastedPercentage  *prometheus.Desc
	internedStringsUsageBufferSize      *prometheus.Desc
	internedStringsUsageUsedMemory      *prometheus.Desc
	internedStringsUsageFreeMemory      *prometheus.Desc
	internedStringsUsageNumberOfStrings *prometheus.Desc
	statisticsNumCachedScripts          *prometheus.Desc
	statisticsNumCachedKeys             *prometheus.Desc
	statisticsMaxCachedKeys             *prometheus.Desc
	statisticsHits                      *prometheus.Desc
	statisticsStartTime                 *prometheus.Desc
	statisticsLastRestartTime           *prometheus.Desc
	statisticsOomRestarts               *prometheus.Desc
	statisticsHashRestarts              *prometheus.Desc
	statisticsManualRestarts            *prometheus.Desc
	statisticsMisses                    *prometheus.Desc
	statisticsBlacklistMisses           *prometheus.Desc
	statisticsBlacklistMissRatio        *prometheus.Desc
	statisticsHitRate                   *prometheus.Desc
}

func (c *opcacheCollector) Describe(ch chan<- *prometheus.Desc) {
	ch <- c.enabled
	ch <- c.cacheFull
	ch <- c.restartPending
	ch <- c.restartInProgress
	ch <- c.memoryUsageUsedMemory
	ch <- c.memoryUsageFreeMemory
	ch <- c.memoryUsageWastedMemory
	ch <- c.memoryUsageCurrentWastedPercentage
	ch <- c.internedStringsUsageBufferSize
	ch <- c.internedStringsUsageUsedMemory
	ch <- c.internedStringsUsageFreeMemory
	ch <- c.internedStringsUsageNumberOfStrings
	ch <- c.statisticsNumCachedScripts
	ch <- c.statisticsNumCachedKeys
	ch <- c.statisticsMaxCachedKeys
	ch <- c.statisticsHits
	ch <- c.statisticsStartTime
	ch <- c.statisticsLastRestartTime
	ch <- c.statisticsOomRestarts
	ch <- c.statisticsHashRestarts
	ch <- c.statisticsManualRestarts
	ch <- c.statisticsMisses
	ch <- c.statisticsBlacklistMisses
	ch <- c.statisticsBlacklistMissRatio
	ch <- c.statisticsHitRate
}

func (c *opcacheCollector) Collect(ch chan<- prometheus.Metric) {
	if res, err := c.collect(); err == nil {
		ch <- gauge(c.enabled, float64(generic.Ternary(res.Status.Enabled, 1, 0)))
		ch <- gauge(c.cacheFull, float64(generic.Ternary(res.Status.CacheFull, 1, 0)))
		ch <- gauge(c.restartPending, float64(generic.Ternary(res.Status.RestartPending, 1, 0)))
		ch <- gauge(c.restartInProgress, float64(generic.Ternary(res.Status.RestartInProgress, 1, 0)))
		ch <- gauge(c.memoryUsageUsedMemory, float64(res.Status.MemoryUsage.UsedMemory))
		ch <- gauge(c.memoryUsageFreeMemory, float64(res.Status.MemoryUsage.FreeMemory))
		ch <- gauge(c.memoryUsageWastedMemory, float64(res.Status.MemoryUsage.WastedMemory))
		ch <- gauge(c.memoryUsageCurrentWastedPercentage, float64(res.Status.MemoryUsage.CurrentWastedPercentage))
		ch <- gauge(c.internedStringsUsageBufferSize, float64(res.Status.InternedStringsUsage.BufferSize))
		ch <- gauge(c.internedStringsUsageUsedMemory, float64(res.Status.InternedStringsUsage.UsedMemory))
		ch <- gauge(c.internedStringsUsageFreeMemory, float64(res.Status.InternedStringsUsage.FreeMemory))
		ch <- gauge(c.internedStringsUsageNumberOfStrings, float64(res.Status.InternedStringsUsage.NumberOfStrings))
		ch <- gauge(c.statisticsNumCachedScripts, float64(res.Status.OpcacheStatistics.NumCachedScripts))
		ch <- gauge(c.statisticsNumCachedKeys, float64(res.Status.OpcacheStatistics.NumCachedKeys))
		ch <- gauge(c.statisticsMaxCachedKeys, float64(res.Status.OpcacheStatistics.MaxCachedKeys))
		ch <- gauge(c.statisticsHits, float64(res.Status.OpcacheStatistics.Hits))
		ch <- gauge(c.statisticsStartTime, float64(res.Status.OpcacheStatistics.StartTime))
		ch <- gauge(c.statisticsLastRestartTime, float64(res.Status.OpcacheStatistics.LastRestartTime))
		ch <- gauge(c.statisticsOomRestarts, float64(res.Status.OpcacheStatistics.OomRestarts))
		ch <- gauge(c.statisticsHashRestarts, float64(res.Status.OpcacheStatistics.HashRestarts))
		ch <- gauge(c.statisticsManualRestarts, float64(res.Status.OpcacheStatistics.ManualRestarts))
		ch <- gauge(c.statisticsMisses, float64(res.Status.OpcacheStatistics.Misses))
		ch <- gauge(c.statisticsBlacklistMisses, float64(res.Status.OpcacheStatistics.BlacklistMisses))
		ch <- gauge(c.statisticsBlacklistMissRatio, float64(res.Status.OpcacheStatistics.BlacklistMissRatio))
		ch <- gauge(c.statisticsHitRate, res.Status.OpcacheStatistics.HitRate)
	} else {
		logrus.WithField("collector", "opcache_status").Error(err)
	}
}

func (c *opcacheCollector) collect() (opcacheStatus, error) {
	url := "http://127.0.0.1" + generic.Default(os.Getenv("NGINX_SERVER_OPTS_STATUS_OPCACHE_PATH"), "/_status/opcache")

	res, err := do(http.MethodGet, url, http.Header{header.UserAgent: []string{"docker-php"}}, nil)
	if err != nil {
		return opcacheStatus{}, err
	}
	defer res.Body.Close()

	var status opcacheStatus

	if err = json.NewDecoder(res.Body).Decode(&status); err != nil {
		return opcacheStatus{}, err
	}

	return status, nil
}

type opcacheStatus struct {
	Configuration struct {
		Directives map[string]any `json:"directives"`
		Version    struct {
			Version            string `json:"version"`
			OpcacheProductName string `json:"opcache_product_name"`
		} `json:"version"`
		Blacklist []any `json:"blacklist"`
	} `json:"configuration"`
	Status struct {
		Enabled           bool `json:"opcache_enabled"`
		CacheFull         bool `json:"cache_full"`
		RestartPending    bool `json:"restart_pending"`
		RestartInProgress bool `json:"restart_in_progress"`
		MemoryUsage       struct {
			UsedMemory              int64   `json:"used_memory"`
			FreeMemory              int64   `json:"free_memory"`
			WastedMemory            int64   `json:"wasted_memory"`
			CurrentWastedPercentage float64 `json:"current_wasted_percentage"`
		} `json:"memory_usage"`
		InternedStringsUsage struct {
			BufferSize      int64 `json:"buffer_size"`
			UsedMemory      int64 `json:"used_memory"`
			FreeMemory      int64 `json:"free_memory"`
			NumberOfStrings int64 `json:"number_of_strings"`
		} `json:"interned_strings_usage"`
		OpcacheStatistics struct {
			NumCachedScripts   int64   `json:"num_cached_scripts"`
			NumCachedKeys      int64   `json:"num_cached_keys"`
			MaxCachedKeys      int64   `json:"max_cached_keys"`
			Hits               int64   `json:"hits"`
			StartTime          int64   `json:"start_time"`
			LastRestartTime    int64   `json:"last_restart_time"`
			OomRestarts        int64   `json:"oom_restarts"`
			HashRestarts       int64   `json:"hash_restarts"`
			ManualRestarts     int64   `json:"manual_restarts"`
			Misses             int64   `json:"misses"`
			BlacklistMisses    int64   `json:"blacklist_misses"`
			BlacklistMissRatio int64   `json:"blacklist_miss_ratio"`
			HitRate            float64 `json:"opcache_hit_rate"`
		} `json:"opcache_statistics"`
		PreloadStatistics struct {
			MemoryConsumption int64    `json:"memory_consumption"`
			Classes           []string `json:"classes"`
			Functions         []string `json:"functions"`
			Scripts           []string `json:"scripts"`
		} `json:"preload_statistics"`
		Scripts map[string]struct {
			FullPath          string `json:"full_path"`
			Hits              int64  `json:"hits"`
			MemoryConsumption int64  `json:"memory_consumption"`
			LastUsed          string `json:"last_used"`
			LastUsedTimestamp Unix   `json:"last_used_timestamp"`
		} `json:"scripts"`
		Jit struct {
			Enabled    bool  `json:"enabled"`
			On         bool  `json:"on"`
			Kind       int64 `json:"kind"`
			OptLevel   int64 `json:"opt_level"`
			OptFlags   int64 `json:"opt_flags"`
			BufferSize int64 `json:"buffer_size"`
			BufferFree int64 `json:"buffer_free"`
		} `json:"jit"`
	} `json:"status"`
}
