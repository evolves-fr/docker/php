package collectors

import "github.com/prometheus/client_golang/prometheus"

func Up(namespace string) prometheus.Collector {
	return &upCollector{
		up: prometheus.NewDesc(
			prometheus.BuildFQName(namespace, "", "up"),
			"Information about the instance.",
			nil,
			nil,
		),
	}
}

type upCollector struct {
	up *prometheus.Desc
}

func (c *upCollector) Describe(ch chan<- *prometheus.Desc) {
	ch <- c.up
}

func (c *upCollector) Collect(ch chan<- prometheus.Metric) {
	ch <- gauge(c.up, 1)
}
