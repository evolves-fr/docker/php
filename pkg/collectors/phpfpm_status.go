package collectors

import (
	"encoding/json"
	"github.com/sirupsen/logrus"
	"net/http"
	"os"

	"github.com/prometheus/client_golang/prometheus"
	"gopkg.org/generic"
	"gopkg.org/header"
)

func PHPFPMStatus(namespace string) prometheus.Collector {
	return &phpfpmStatusCollector{
		startSince: prometheus.NewDesc(
			prometheus.BuildFQName(namespace, "fpm", "start_since"),
			"The time in seconds since the process pool was last started.",
			[]string{"pool", "process_manager"},
			nil,
		),
		acceptedConn: prometheus.NewDesc(
			prometheus.BuildFQName(namespace, "fpm", "accepted_conn"),
			"The total number of accepted connections.",
			[]string{"pool", "process_manager"},
			nil,
		),
		listenQueue: prometheus.NewDesc(
			prometheus.BuildFQName(namespace, "fpm", "listen_queue"),
			"The number of requests (backlog) currently waiting for a free process.",
			[]string{"pool", "process_manager"},
			nil,
		),
		maxListenQueue: prometheus.NewDesc(
			prometheus.BuildFQName(namespace, "fpm", "max_listen_queue"),
			"The maximum number of requests seen in the listen queue at any one time.",
			[]string{"pool", "process_manager"},
			nil,
		),
		listenQueueLen: prometheus.NewDesc(
			prometheus.BuildFQName(namespace, "fpm", "listen_queue_len"),
			"The maximum allowed size of the listen queue.",
			[]string{"pool", "process_manager"},
			nil,
		),
		idleProcesses: prometheus.NewDesc(
			prometheus.BuildFQName(namespace, "fpm", "idle_processes"),
			"The number of processes that are currently idle (waiting for requests).",
			[]string{"pool", "process_manager"},
			nil,
		),
		activeProcesses: prometheus.NewDesc(
			prometheus.BuildFQName(namespace, "fpm", "active_processes"),
			"The number of processes that are currently processing requests.",
			[]string{"pool", "process_manager"},
			nil,
		),
		totalProcesses: prometheus.NewDesc(
			prometheus.BuildFQName(namespace, "fpm", "total_processes"),
			"The current total number of processes.",
			[]string{"pool", "process_manager"},
			nil,
		),
		maxActiveProcesses: prometheus.NewDesc(
			prometheus.BuildFQName(namespace, "fpm", "max_active_processes"),
			"The maximum number of concurrently active processes.",
			[]string{"pool", "process_manager"},
			nil,
		),
		maxChildrenReached: prometheus.NewDesc(
			prometheus.BuildFQName(namespace, "fpm", "max_children_reached"),
			"Has the maximum number of processes ever been reached? If so the displayed value is 1 otherwise the value is 0.",
			[]string{"pool", "process_manager"},
			nil,
		),
		slowRequests: prometheus.NewDesc(
			prometheus.BuildFQName(namespace, "fpm", "slow_requests"),
			"The total number of requests that have hit the configured request_slowlog_timeout.",
			[]string{"pool", "process_manager"},
			nil,
		),
	}
}

type phpfpmStatusCollector struct {
	startSince         *prometheus.Desc
	acceptedConn       *prometheus.Desc
	listenQueue        *prometheus.Desc
	maxListenQueue     *prometheus.Desc
	listenQueueLen     *prometheus.Desc
	idleProcesses      *prometheus.Desc
	activeProcesses    *prometheus.Desc
	totalProcesses     *prometheus.Desc
	maxActiveProcesses *prometheus.Desc
	maxChildrenReached *prometheus.Desc
	slowRequests       *prometheus.Desc
}

func (c *phpfpmStatusCollector) Describe(ch chan<- *prometheus.Desc) {
	ch <- c.startSince
	ch <- c.acceptedConn
	ch <- c.listenQueue
	ch <- c.maxListenQueue
	ch <- c.listenQueueLen
	ch <- c.idleProcesses
	ch <- c.activeProcesses
	ch <- c.totalProcesses
	ch <- c.maxActiveProcesses
	ch <- c.maxChildrenReached
	ch <- c.slowRequests
}

func (c *phpfpmStatusCollector) Collect(ch chan<- prometheus.Metric) {
	if res, err := c.collect(); err == nil {
		labels := []string{res.Pool, res.ProcessManager}

		ch <- counter(c.startSince, float64(res.StartSince), labels...)
		ch <- counter(c.acceptedConn, float64(res.AcceptedConn), labels...)
		ch <- gauge(c.listenQueue, float64(res.ListenQueue), labels...)
		ch <- counter(c.maxListenQueue, float64(res.MaxListenQueue), labels...)
		ch <- gauge(c.listenQueueLen, float64(res.ListenQueueLen), labels...)
		ch <- gauge(c.idleProcesses, float64(res.IdleProcesses), labels...)
		ch <- gauge(c.activeProcesses, float64(res.ActiveProcesses), labels...)
		ch <- gauge(c.totalProcesses, float64(res.TotalProcesses), labels...)
		ch <- counter(c.maxActiveProcesses, float64(res.MaxActiveProcesses), labels...)
		ch <- counter(c.maxChildrenReached, float64(res.MaxChildrenReached), labels...)
		ch <- counter(c.slowRequests, float64(res.SlowRequests), labels...)
	} else {
		logrus.WithField("collector", "phpfpm_status").Error(err)
	}
}

func (c *phpfpmStatusCollector) collect() (phpfpmStatus, error) {
	url := "http://127.0.0.1" + generic.Default(os.Getenv("NGINX_SERVER_OPTS_STATUS_FPM_PATH"), "/_status/fpm") + "?json"

	res, err := do(http.MethodGet, url, http.Header{header.UserAgent: []string{"docker-php"}}, nil)
	if err != nil {
		return phpfpmStatus{}, err
	}
	defer res.Body.Close()

	var status phpfpmStatus

	if err = json.NewDecoder(res.Body).Decode(&status); err != nil {
		return phpfpmStatus{}, err
	}

	return status, nil
}

type phpfpmStatus struct {
	Pool               string `json:"pool"`
	ProcessManager     string `json:"process manager"`
	StartTime          uint64 `json:"start time"`
	StartSince         uint64 `json:"start since"`
	AcceptedConn       uint64 `json:"accepted conn"`
	ListenQueue        uint64 `json:"listen queue"`
	MaxListenQueue     uint64 `json:"max listen queue"`
	ListenQueueLen     uint64 `json:"listen queue len"`
	IdleProcesses      uint64 `json:"idle processes"`
	ActiveProcesses    uint64 `json:"active processes"`
	TotalProcesses     uint64 `json:"total processes"`
	MaxActiveProcesses uint64 `json:"max active processes"`
	MaxChildrenReached uint64 `json:"max children reached"`
	SlowRequests       uint64 `json:"slow requests"`
}
