package main

import (
	"os"

	"github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
	"gitlab.com/evolves-fr/docker/php/internal"
	"gitlab.com/evolves-fr/gommon"
)

var Version = "0.0.0"

var server internal.Server

func init() {
	logrus.SetOutput(os.Stdout)
	logrus.SetLevel(logrus.InfoLevel)
	logrus.SetFormatter(&logrus.TextFormatter{FullTimestamp: true, QuoteEmptyFields: true})
}

func main() {
	flags := []cli.Flag{
		&cli.BoolFlag{Name: "debug", Usage: "Debug mode", Aliases: []string{"d"}, EnvVars: []string{"ENTRYPOINT_DEBUG"}},
		&cli.BoolFlag{Name: "json", Usage: "JSON logs output", Aliases: []string{"j"}, EnvVars: []string{"ENTRYPOINT_JSON"}},
	}

	commands := cli.Commands{
		{
			Name:        "serve",
			UsageText:   "entrypoint serve",
			Usage:       "Start web server",
			Description: "Run web server with PHP-FPM and Nginx",
			Action:      serveCommand,
		},
		{
			Name:            "cachetool",
			Usage:           "Manage cache in the CLI",
			Description:     "CacheTool allows you to work with APCu, OPcache, and the file status cache through the CLI.",
			Action:          cacheToolCommand,
			SkipFlagParsing: true,
		},
	}

	app := &cli.App{
		Name:            "entrypoint",
		Usage:           "Docker  entrypoint",
		Copyright:       "Evolves SAS",
		Version:         Version,
		Authors:         []*cli.Author{{Name: "Thomas FOURNIER", Email: "tfournier@evolves.fr"}},
		Flags:           flags,
		HideHelp:        true,
		Before:          beforeCommand,
		CommandNotFound: defaultCommand,
		Commands:        commands,
	}

	if err := app.Run(os.Args); err != nil {
		logrus.Fatal(err)
	}
}

func beforeCommand(c *cli.Context) error {
	if c.Bool("debug") {
		logrus.SetLevel(logrus.DebugLevel)
	}

	if c.Bool("json") {
		logrus.SetFormatter(&logrus.JSONFormatter{})
	}

	server = internal.NewServer()

	return server.Init()
}

func defaultCommand(c *cli.Context, _ string) {
	if err := server.Exec("command", c.Args().First(), c.Args().Tail()...); err != nil {
		logrus.Fatal(err)
	}
}

func serveCommand(*cli.Context) error {
	return server.Run()
}

func cacheToolCommand(c *cli.Context) error {
	args := c.Args().Slice()
	if fpmListener := os.Getenv("FPM_WWW_LISTEN"); !gommon.Empty(fpmListener) {
		args = append([]string{"--fcgi=\"" + fpmListener + "\""}, args...)
	}

	return server.Exec("cachetool", "cachetool", args...)
}
